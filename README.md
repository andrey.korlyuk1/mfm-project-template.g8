mfm-service-template.g8
===

A [Giter8](http://www.foundweekends.org/giter8/) template for creating mfm-service-template.g8



How to create a new project based on the template?
---

* Go to directory where you want to create the template
* Decide your project name (the hardest part :))
* Run the command

    `sbt new {GITHUB_USER}/mfm-service-template.g8 --branch master --package="com.moneyfarm" -o mfm-service-template`

or    

* Install g8 commandline tool (http://www.foundweekends.org/giter8/setup.html)
* Run the command

    `g8 {GITHUB_USER}/mfm-service-template.g8 --branch master --package="com.moneyfarm" -o mfm-service-template`
    
and then
    
    cd mfm-service-template
    git init
	git add .
	git commit -m start
  
* Test generated project using command 

    `sbt test`
    

How to test the template and generate an example project?
---

* Run `./test.sh` 

An example project will be then created and tested in `target/sandbox/mfm-service-template`

How to modify the template?
---

 * review template sources in `/src/main/g8`
 * modify files as you need, but be careful about placeholders, paths and so on
 * run `./test.sh` in template root to validate your changes
 
or (safer) ...

* run `./test.sh` first
* open `target/sandbox/mfm-service-template` in your preferred IDE, 
* modify the generated example project as you wish, 
* build and test it as usual, you can run `sbt test`,
* when you are done switch back to the template root
* run `./update-g8.sh` in order to port your changes back to the template.
* run `./test.sh` again to validate your changes

What is in the template?
--

Assuming the command above 
the template will supply the following values for the placeholders:

    $packaged$ -> com/moneyfarm
	$package$ -> com.moneyfarm

and produce the folders and files as shown below:

    ├── .env.sample
	├── .gitignore
	├── .gitlab-ci.yml
	├── .scalafmt.conf
	├── build.sbt
	├── CHANGELOG.md
	├── Dockerfile
	├── project.yaml
	├── README.md
	├── src
	│   ├── it
	│   │   ├── resources
	│   │   │   ├── application.conf
	│   │   │   └── logback-test.xml
	│   │   │
	│   │   └── scala
	│   │       └── com
	│   │           └── moneyfarm
	│   │               └── investment
	│   │                   └── it
	│   │                       └── testing
	│   │                           ├── ActorSystemSpec.scala
	│   │                           └── FixedClock.scala
	│   │
	│   ├── main
	│   │   ├── protobuf
	│   │   │   └── investmentOrderEvents.proto
	│   │   │
	│   │   ├── resources
	│   │   │   ├── application.conf
	│   │   │   ├── db
	│   │   │   │   └── migration
	│   │   │   │       └── V001__Baseline.sql
	│   │   │   │
	│   │   │   └── logback.xml
	│   │   │
	│   │   └── scala
	│   │       └── com
	│   │           └── moneyfarm
	│   │               └── investment
	│   │                   ├── Config.scala
	│   │                   ├── dal
	│   │                   │   ├── infra
	│   │                   │   │   ├── db
	│   │                   │   │   │   ├── DB.scala
	│   │                   │   │   │   ├── InvestmentOrderDB.scala
	│   │                   │   │   │   ├── model
	│   │                   │   │   │   │   └── InvestmentOrderEntity.scala
	│   │                   │   │   │   │
	│   │                   │   │   │   ├── StringKeyedModelDao.scala
	│   │                   │   │   │   ├── TypeMode.scala
	│   │                   │   │   │   └── TypeModeWrapper.scala
	│   │                   │   │   │
	│   │                   │   │   └── rest
	│   │                   │   │       └── lisa
	│   │                   │   │           ├── LisaApi.scala
	│   │                   │   │           ├── LisaProtocol.scala
	│   │                   │   │           └── model
	│   │                   │   │               └── LedgerPortfolioContract.scala
	│   │                   │   │
	│   │                   │   └── InvestmentOrderDao.scala
	│   │                   │
	│   │                   ├── Dependencies.scala
	│   │                   ├── event
	│   │                   │   └── consumer
	│   │                   │       └── InvestmentOrderStreamTopology.scala
	│   │                   │
	│   │                   ├── Investment.scala
	│   │                   ├── mapper
	│   │                   │   └── InvestmentOrderMapper.scala
	│   │                   │
	│   │                   ├── model
	│   │                   │   ├── Command.scala
	│   │                   │   ├── CreateInvestmentOrder.scala
	│   │                   │   ├── Event.scala
	│   │                   │   ├── InvestmentOrder.scala
	│   │                   │   ├── InvestmentOrderFilter.scala
	│   │                   │   ├── InvestmentOrderId.scala
	│   │                   │   └── UserId.scala
	│   │                   │
	│   │                   ├── routes
	│   │                   │   ├── external
	│   │                   │   │   ├── InvestmentOrdersRoutes.scala
	│   │                   │   │   ├── json
	│   │                   │   │   │   └── InvestmentOrderProtocol.scala
	│   │                   │   │   │
	│   │                   │   │   ├── mapper
	│   │                   │   │   │   ├── CreateInvestmentOrderMapper.scala
	│   │                   │   │   │   ├── InvestmentOrderListViewMapper.scala
	│   │                   │   │   │   └── InvestmentOrderViewMapper.scala
	│   │                   │   │   │
	│   │                   │   │   ├── model
	│   │                   │   │   │   ├── CreateInvestmentOrderRequest.scala
	│   │                   │   │   │   ├── InvestmentOrderListView.scala
	│   │                   │   │   │   └── InvestmentOrderView.scala
	│   │                   │   │   │
	│   │                   │   │   ├── Permissions.scala
	│   │                   │   │   └── Routes.scala
	│   │                   │   │
	│   │                   │   ├── handler
	│   │                   │   │   ├── CustomExceptionHandler.scala
	│   │                   │   │   └── CustomRejectionHandler.scala
	│   │                   │   │
	│   │                   │   └── internal
	│   │                   │       ├── Endpoints.scala
	│   │                   │       ├── JobsRoutes.scala
	│   │                   │       ├── json
	│   │                   │       │   └── UserInvestmentOrderProtocol.scala
	│   │                   │       │
	│   │                   │       ├── mapper
	│   │                   │       │   ├── InvestmentOrderListViewMapper.scala
	│   │                   │       │   └── InvestmentOrderViewMapper.scala
	│   │                   │       │
	│   │                   │       ├── model
	│   │                   │       │   ├── InvestmentOrderListView.scala
	│   │                   │       │   ├── InvestmentOrderView.scala
	│   │                   │       │   └── TriggerDailyJobRequest.scala
	│   │                   │       │
	│   │                   │       ├── Routes.scala
	│   │                   │       └── UserInvestmentOrdersRoutes.scala
	│   │                   │
	│   │                   ├── Routes.scala
	│   │                   └── service
	│   │                       └── InvestmentOrderService.scala
	│   │
	│   └── test
	│       ├── resources
	│       │   ├── application.conf
	│       │   └── logback-test.xml
	│       │
	│       └── scala
	│           └── com
	│               └── moneyfarm
	│                   └── investment
	│                       ├── application
	│                       │   └── PortfoliosStreamsSpec.scala
	│                       │
	│                       ├── dal
	│                       │   ├── CashFlowDaoSpec.scala
	│                       │   ├── infra
	│                       │   │   ├── db
	│                       │   │   │   └── model
	│                       │   │   │       ├── InvestmentEntitySpec.scala
	│                       │   │   │       └── InvestmentOrderEntitySpec.scala
	│                       │   │   │
	│                       │   │   └── rest
	│                       │   │       ├── lisa
	│                       │   │       │   └── model
	│                       │   │       │       └── CreateCashFlowRequestContractSpec.scala
	│                       │   │       │
	│                       │   │       └── portfolio
	│                       │   │           ├── mapper
	│                       │   │           │   └── FundingRequestValidationContractMapperSpec.scala
	│                       │   │           │
	│                       │   │           └── model
	│                       │   │               └── enums
	│                       │   │                   └── mapper
	│                       │   │                       └── ValidateForFundingFrequencyMapperSpec.scala
	│                       │   │
	│                       │   ├── InvestmentDaoSpec.scala
	│                       │   ├── InvestmentOrderDaoSpec.scala
	│                       │   ├── LedgerPortfolioDaoSpec.scala
	│                       │   └── PortfolioDaoSpec.scala
	│                       │
	│                       ├── event
	│                       │   ├── PortfolioEventHandlerPoliciesSpec.scala
	│                       │   └── PortfolioEventHandlerSpec.scala
	│                       │
	│                       ├── routes
	│                       │   ├── DefaultRoutesSpec.scala
	│                       │   ├── external
	│                       │   │   ├── fixture
	│                       │   │   │   └── InvestmentOrderFixture.scala
	│                       │   │   │
	│                       │   │   ├── InvestmentOrdersRoutesSpec.scala
	│                       │   │   ├── mapper
	│                       │   │   │   ├── CreateInvestmentOrderMapperSpec.scala
	│                       │   │   │   ├── InvalidParametersProblemDetailMapperSpec.scala
	│                       │   │   │   ├── InvestmentOrderListViewMapperSpec.scala
	│                       │   │   │   └── InvestmentOrderViewMapperSpec.scala
	│                       │   │   │
	│                       │   │   └── validation
	│                       │   │       └── CreateInvestmentOrderRequestValidatorSpec.scala
	│                       │   │
	│                       │   └── internal
	│                       │       ├── fixture
	│                       │       │   └── InvestmentOrderFixture.scala
	│                       │       │
	│                       │       ├── JobsRoutesSpec.scala
	│                       │       ├── mapper
	│                       │       │   ├── InvestmentOrderListViewMapperSpec.scala
	│                       │       │   └── InvestmentOrderViewMapperSpec.scala
	│                       │       │
	│                       │       └── UserInvestmentOrdersRoutesSpec.scala
	│                       │
	│                       ├── service
	│                       │   ├── CashServiceSpec.scala
	│                       │   ├── InvestmentExecutionServiceSpec.scala
	│                       │   └── InvestmentOrderServiceSpec.scala
	│                       │
	│                       └── testing
	│                           ├── ActorSystemSpec.scala
	│                           ├── CustomMatchers.scala
	│                           ├── FixedClock.scala
	│                           ├── PatienceConfig.scala
	│                           └── RouteSpec.scala
	│
	└── version