package $package$.$name;format="normalize"$.routes.internal.model

import java.util.UUID

case class InvestmentOrderView(id: UUID, active: Boolean)
