package $package$.$name;format="normalize"$.routes.internal.model

import java.time.LocalDate

case class TriggerDailyJobRequest(date: Option[LocalDate])
