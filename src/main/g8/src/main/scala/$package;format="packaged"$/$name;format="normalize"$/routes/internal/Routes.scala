package $package$.$name;format="normalize"$.routes.internal

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import $package$.$name;format="normalize"$.routes.internal.Endpoints.{getJsonDocs, jsonDocs, yamlDocs}
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import sttp.tapir.swagger.akkahttp.SwaggerAkka

import scala.concurrent.Future

trait Routes extends UserInvestmentOrdersRoutes with JobsRoutes {

  private def jsonDocsRoute: Route = AkkaHttpServerInterpreter.toRoute(getJsonDocs) { _ =>
    Future.successful(Right(jsonDocs))
  }

  val internalRoutes: Route =
    userInvestmentOrdersRoutes $if(useBackgroundJobs.truthy)$~ jobsRoutes$endif$ ~ jsonDocsRoute ~ new SwaggerAkka(yaml = yamlDocs, contextPath = "tapir-docs").routes

}
