package $package$.$name;format="normalize"$.routes.external

object Permissions {
  val createInvestmentOrder = "create:customer:investment-orders"
  val readInvestmentOrders = "read:customer:investment-orders"
}
