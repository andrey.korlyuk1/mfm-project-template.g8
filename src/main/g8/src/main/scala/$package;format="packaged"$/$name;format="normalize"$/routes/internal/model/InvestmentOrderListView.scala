package $package$.$name;format="normalize"$.routes.internal.model

case class InvestmentOrderListView(items: List[InvestmentOrderView])
