package $package$.$name;format="normalize"$.model

case class CreateInvestmentOrder(userId: UserId, active: Boolean)
