package $package$.$name;format="normalize"$.routes.external.json

import $package$.commons.http.problem.model.ProblemDetailJsonProtocol
import $package$.$name;format="normalize"$.routes.external.model._
import $package$.json.spray.CommonJsonProtocol
import spray.json.DefaultJsonProtocol

trait InvestmentOrderProtocol extends DefaultJsonProtocol with CommonJsonProtocol with ProblemDetailJsonProtocol {

  implicit val createInvestmentOrderRequestFormat = jsonFormat1(CreateInvestmentOrderRequest)
  implicit val investmentOrderViewFormat = jsonFormat2(InvestmentOrderView)
  implicit val investmentOrderListViewFormat = jsonFormat1(InvestmentOrderListView)

}
