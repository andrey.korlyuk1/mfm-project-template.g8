package $package$.$name;format="normalize"$.dal.infra.db

import org.squeryl.KeyedEntity
import org.squeryl.dsl.TString

trait StringKeyedModelDao[A <: KeyedEntity[String]] extends $package$.squeryl.dao.ModelDao[String, A, TString] with TypeModeWrapper[A]
