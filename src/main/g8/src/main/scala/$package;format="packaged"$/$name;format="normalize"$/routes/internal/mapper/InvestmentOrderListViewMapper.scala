package $package$.$name;format="normalize"$.routes.internal.mapper

import $package$.$name;format="normalize"$.model.InvestmentOrder
import $package$.$name;format="normalize"$.routes.internal.model.InvestmentOrderListView

object InvestmentOrderListViewMapper {
  def from(modelList: List[InvestmentOrder]): InvestmentOrderListView =
    InvestmentOrderListView(modelList.map(InvestmentOrderViewMapper.from))

}
