package $package$.$name;format="normalize"$.routes.external.mapper

import $package$.$name;format="normalize"$.model.{CreateInvestmentOrder, UserId}
import $package$.$name;format="normalize"$.routes.external.model.CreateInvestmentOrderRequest

object CreateInvestmentOrderMapper {
  def from(request: CreateInvestmentOrderRequest, userId: UserId): CreateInvestmentOrder =
    CreateInvestmentOrder(
      userId = userId,
      active = request.active
    )

}
