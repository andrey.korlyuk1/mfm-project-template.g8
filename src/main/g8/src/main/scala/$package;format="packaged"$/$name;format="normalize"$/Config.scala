package $package$.$name;format="normalize"$

import $package$.commons.config.ServiceConfig
$if(useKafka.truthy)$
import $package$.commons.kafka.Topic
import $package$.commons.kafka.config.KafkaConfig
import $package$.events.{InvestmentOrderEvent, InvestmentOrderEvents}
$endif$

import com.zaxxer.hikari.HikariDataSource

import java.util.Properties
import javax.sql.DataSource
import scala.concurrent.duration.{FiniteDuration, _}

object Config extends ServiceConfig(BuildInfo.name) {

  lazy val DocumentationServiceName: String = BuildInfo.name
  lazy val DocumentationServiceVersion: String = BuildInfo.version

  lazy val LisaUrl: String = config.getString("lisa.url")

  lazy val AuthJwksSourceUrl: String = getString("auth.jwks.url")

  $if(setupDB.truthy)$
  lazy val DatabaseUrl: String = getString("database.url")
  private lazy val DatabaseDriver = getOptionalString("database.driver").getOrElse(classOf[com.mysql.jdbc.Driver].getName)
  private lazy val DatabaseUsername = getString("database.username")
  private lazy val DatabasePassword = getString("database.password")
  private lazy val DatabaseConnectionMin =
    getOptionalInt("database.connection.min").getOrElse(0)
  private lazy val DatabaseConnectionMax =
    getOptionalInt("database.connection.max").getOrElse(4)

  lazy val DataSource: DataSource = {
    Class.forName(DatabaseDriver)
    val ds = new HikariDataSource()
    ds.setDriverClassName(DatabaseDriver)
    ds.setJdbcUrl(DatabaseUrl)
    ds.setUsername(DatabaseUsername)
    ds.setPassword(DatabasePassword)
    ds.setMinimumIdle(DatabaseConnectionMin)
    ds.setMaximumPoolSize(Config.DatabaseConnectionMax)
    ds.setIdleTimeout(360000) // ms
    ds.setConnectionTestQuery("SELECT 1")
    ds
  }
  $endif$

  // job retention
  val JobRetentionAge: FiniteDuration = 2.days
  $if(useKafka.truthy)$
  final object Kafka extends KafkaConfig {
    override protected def getConfig(key: String): com.typesafe.config.Config = config.getConfig(key)
    override protected def secret(key: String): String = getString(key)

    object Topics {
      val InvestmentOrdersTopicName: String = getString("kafka.topics.investment-orders")
      lazy val investmentOrdersTopic: Topic[String, InvestmentOrderEvent] =
        Topic.mkStringKeyedTopic[InvestmentOrderEvent, InvestmentOrderEvents.InvestmentOrderEvent](
          name = InvestmentOrdersTopicName,
          valueCompanion = InvestmentOrderEvent,
          schemaRegistryClient = None,
          serdeConfig = SchemaRegistry.SettingsMap
        )
    }

    object Streams {
      val InvestmentOrderProperties: Properties = kafkaProperties("kafka.streams.investment-orders")
    }
  }

  val PortfoliosStreamsProcessingTimeout: Duration = getDuration("portfolios-streams-processing.timeout")
  $endif$

  override lazy val toString: String =
    s"""Configuration [
       |  \${super.toString}
       |  ENDPOINTS:
       |    - Lisa: \$LisaUrl
       |
       |  DATABASE:
       |    - Driver: \$DatabaseDriver
       |    - URL: \$DatabaseUrl
       |    - Username: \$DatabaseUsername
       |    - Connections: [\$DatabaseConnectionMin:\$DatabaseConnectionMax]
       |
       |  AUTH:
       |    - JWKS: \$AuthJwksSourceUrl
       |
       |  JOBS
       |    - RetentionAge: \$JobRetentionAge
       |    
       |  $if(useKafka.truthy)$\${Kafka.printConfig()}$endif$
       |]""".stripMargin
}
