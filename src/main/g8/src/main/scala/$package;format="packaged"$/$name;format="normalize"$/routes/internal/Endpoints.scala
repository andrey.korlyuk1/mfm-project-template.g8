package $package$.$name;format="normalize"$.routes.internal

import $package$.$name;format="normalize"$.Config
import $package$.$name;format="normalize"$.routes.internal.json.UserInvestmentOrderProtocol
import $package$.$name;format="normalize"$.routes.internal.model.InvestmentOrderListView
import io.circe.syntax._
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.docs.openapi._
import sttp.tapir.generic.SchemaDerivation
import sttp.tapir.json.spray.TapirJsonSpray
import sttp.tapir.openapi.OpenAPI
import sttp.tapir.openapi.circe._
import sttp.tapir.openapi.circe.yaml._

import java.time.LocalDate

sealed trait ErrorInfo
case object NotFound extends ErrorInfo

// format: off
object Endpoints extends TapirJsonSpray with UserInvestmentOrderProtocol with OpenAPIDocsInterpreter with SchemaDerivation {

  private val baseEndpoint = endpoint.errorOut(emptyOutput).in("users" / path[Long]("userId") / "investment-orders")

  val getUserInvestmentOrders: Endpoint[(Long, Option[Boolean]), Unit, InvestmentOrderListView, Any] =
    baseEndpoint.description("get investment orders")
      .in(query[Option[Boolean]]("active"))
      .get
      .out(jsonBody[InvestmentOrderListView].description("investment order list"))

  val getJsonDocs: Endpoint[Unit, Unit, String, Any] =
    endpoint
      .in("docs" / "v2" / "swagger.json")
      .out(stringBody)

  $if(useBackgroundJobs.truthy)$
  val triggerInvestmentOrdersExecution: Endpoint[Option[LocalDate], Unit, String, Any] = endpoint.in("jobs" / "investment-orders" / "trigger").description("execute investment orders")
    .in(query[Option[LocalDate]]("date"))
    .post
    .out(statusCode(StatusCode.Accepted).and(stringBody.description("investment order list").example("Job status can be viewed at /jobs/:jobId")))
  $endif$
  private val endpoints = List(
    getUserInvestmentOrders,
    $if(useBackgroundJobs.truthy)$triggerInvestmentOrdersExecution$endif$
  )

  val docs: OpenAPI = toOpenAPI(endpoints, s"\${Config.DocumentationServiceName.capitalize} documentation", s"\${Config.DocumentationServiceVersion}")

  val yamlDocs: String = docs.toYaml
  val jsonDocs: String = docs.asJson.deepDropNullValues.toString

}
//format:on