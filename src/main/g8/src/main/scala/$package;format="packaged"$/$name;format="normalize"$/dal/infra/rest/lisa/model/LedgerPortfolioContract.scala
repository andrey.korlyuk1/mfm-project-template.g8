package $package$.$name;format="normalize"$.dal.infra.rest.lisa.model

case class LedgerPortfolioContract(id: Long, parentId: Option[Long], availableCashForInvestment: BigDecimal)
