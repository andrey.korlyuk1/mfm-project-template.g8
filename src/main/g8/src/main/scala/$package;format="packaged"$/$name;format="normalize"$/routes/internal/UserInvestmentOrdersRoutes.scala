package $package$.$name;format="normalize"$.routes.internal

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import $package$.commons.akka.http.directives.LoggingDirectives
import $package$.$name;format="normalize"$.Dependencies
import $package$.$name;format="normalize"$.model.{InvestmentOrderFilter, UserId}
import $package$.$name;format="normalize"$.routes.internal.Endpoints._
import $package$.$name;format="normalize"$.routes.internal.mapper.InvestmentOrderListViewMapper
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter

// this ex example route
trait UserInvestmentOrdersRoutes extends Dependencies with LoggingDirectives {

  private def getUserInvestmentOrdersRoute: Route =
    AkkaHttpServerInterpreter.toDirective(getUserInvestmentOrders).tapply { case ((userId: Long, active: Option[Boolean]), complete) =>
      logRequestBodyResponseBody {
        rejectEmptyResponse {
          complete(
            investmentOrderService
              .getInvestmentOrders(InvestmentOrderFilter(Some(UserId(userId)), active))
              .map(investmentOrderList => Right(InvestmentOrderListViewMapper.from(investmentOrderList)))
          )

        }
      }
    }

  def userInvestmentOrdersRoutes: Route = getUserInvestmentOrdersRoute

}
