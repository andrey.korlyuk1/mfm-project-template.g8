package $package$.$name;format="normalize"$.routes.external.mapper

import $package$.$name;format="normalize"$.model.InvestmentOrder
import $package$.$name;format="normalize"$.routes.external.model.InvestmentOrderView

object InvestmentOrderViewMapper {
  def from(model: InvestmentOrder): InvestmentOrderView =
    InvestmentOrderView(
      id = model.id.value,
      active = model.active
    )
}
