package $package$.$name;format="normalize"$

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import $package$.commons.akka.http.RouteExceptionHandling
import $package$.commons.auth.akka.jwk.{JsonWebKeySetCachedProvider, JsonWebKeySetProvider, JwksCachedProviderConfig}
import $package$.commons.auth.akka.jwt.{AuthorisationBearerJwtExtractor, JwtExtractor}
import $package$.commons.auth.jwt.{JwtDecoder, JwtDynamicDecoder}
import $package$.$name;format="normalize"$.dal.infra.db.{DB, TypeMode}
import $package$.$name;format="normalize"$.routes.external.{Routes => ExternalRoutes}
import $package$.$name;format="normalize"$.routes.handler.{CustomExceptionHandler, CustomRejectionHandler}
import $package$.$name;format="normalize"$.routes.internal.{Routes => InternalRoutes}
import $package$.util.jobs.{DbJobMonitor, JobMonitor}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.duration._

class Routes(defaultRoutes: Route)(implicit val actorSystem: ActorSystem)
    extends StrictLogging
    with RouteExceptionHandling
    with Dependencies
  $if(externalRoutesSupport.truthy)$with ExternalRoutes$endif$
    with InternalRoutes
    with CustomExceptionHandler
    with CustomRejectionHandler {

  protected val DefaultDataWaitTime: FiniteDuration = 1.minute
  $if(externalRoutesSupport.truthy)$
  lazy val jwksProvider =
    new JsonWebKeySetProvider(Config.AuthJwksSourceUrl, logger)
  lazy val cachedJwksProvider = new JsonWebKeySetCachedProvider(jwksProvider, JwksCachedProviderConfig.Default)

  override protected lazy val jwtDecoder: JwtDecoder = new JwtDynamicDecoder(cachedJwksProvider)
  override protected lazy val jwtExtractor: JwtExtractor =
    new AuthorisationBearerJwtExtractor()
  $endif$

  lazy val routes: Route =
    handleExceptions(customExceptionHandler) {
      handleRejections(customRejectionHandler) {
        toStrictEntity(DefaultDataWaitTime) {
          cors() {
            defaultRoutes ~ $if(externalRoutesSupport.truthy)$externalRoutes$endif$ ~ internalRoutes
          }
        }
      }
    }

  $if(useBackgroundJobs.truthy)$
  protected override implicit val jobMonitor: JobMonitor =
    DbJobMonitor.create(DB, TypeMode, maxJobRetention)
  override def maxJobRetention: Duration = Config.JobRetentionAge
  $endif$
}
