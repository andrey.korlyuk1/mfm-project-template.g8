package $package$.$name;format="normalize"$.dal.infra.db

import $package$.$name;format="normalize"$.dal.infra.db.TypeMode._
import $package$.$name;format="normalize"$.dal.infra.db.model.InvestmentOrderEntity
import $package$.squeryl.Schema
import $package$.util.jobs.db.JobMonitorSchema
import $package$.util.jobs.db.entities.{JobInstanceEntity, JobNameEntity}

object DB extends Schema with JobMonitorSchema {

  val investmentOrder = table[InvestmentOrderEntity]("investment_order")

  override val jobInstances = table[JobInstanceEntity]("job_instance")
  on(jobInstances) { jobInstance =>
    declare(
      columns(jobInstance.name, jobInstance.status) are indexed
    )
  }

  override val jobNames = table[JobNameEntity]("job_name")
  on(jobNames) { jobName =>
    declare(
      jobName.name is unique
    )
  }
}
