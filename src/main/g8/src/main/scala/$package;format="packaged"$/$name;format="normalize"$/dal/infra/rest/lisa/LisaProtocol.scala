package $package$.$name;format="normalize"$.dal.infra.rest.lisa

import $package$.$name;format="normalize"$.dal.infra.rest.lisa.model.LedgerPortfolioContract
import $package$.json.spray.CommonJsonProtocol
import spray.json.DefaultJsonProtocol

trait LisaProtocol extends CommonJsonProtocol with DefaultJsonProtocol {

  implicit val ledgerPortfolioContract = jsonFormat3(LedgerPortfolioContract)

}
