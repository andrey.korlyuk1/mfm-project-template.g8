package $package$.$name;format="normalize"$.dal.infra.db

import $package$.squeryl.operations.OperationRequirements
import org.squeryl.KeyedEntity

trait TypeModeWrapper[A <: KeyedEntity[_]] extends OperationRequirements[A] {

  override val typeMode = TypeMode

}
