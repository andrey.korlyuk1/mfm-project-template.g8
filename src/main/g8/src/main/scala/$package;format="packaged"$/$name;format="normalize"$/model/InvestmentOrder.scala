package $package$.$name;format="normalize"$.model

case class InvestmentOrder(id: InvestmentOrderId, userId: UserId, active: Boolean)
