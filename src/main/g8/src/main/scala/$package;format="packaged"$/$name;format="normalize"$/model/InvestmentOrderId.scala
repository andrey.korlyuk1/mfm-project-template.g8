package $package$.$name;format="normalize"$.model

import java.util.UUID

case class InvestmentOrderId(value: UUID) extends AnyVal
