package $package$.$name;format="normalize"$

import akka.actor.ActorSystem
import akka.stream.Materializer
$if(useKafka.truthy)$
import $package$.commons.kafka.streams.KafkaStreamsApp
$endif$
import $package$.commons.rest.akka.AkkaRestClient
import $package$.$name;format="normalize"$.dal._
import $package$.$name;format="normalize"$.dal.infra.db.InvestmentOrderDB
import $package$.$name;format="normalize"$.dal.infra.rest.lisa.LisaApi
$if(useKafka.truthy)$
import $package$.$name;format="normalize"$.event.consumer.{InvestmentOrderEventHandler, InvestmentOrderEventMapper, InvestmentOrderStreamTopology}
$endif$
import $package$.$name;format="normalize"$.service.InvestmentOrderService
import $package$.util.UUIDGenerator

import java.time.Clock
import scala.concurrent.ExecutionContext

trait Dependencies {

  protected implicit def actorSystem: ActorSystem
  protected implicit val materializer: Materializer = Materializer(actorSystem)
  protected implicit val ec: ExecutionContext = actorSystem.dispatchers.lookup("services-dispatcher")

  protected implicit val clock: Clock = Clock.systemDefaultZone()
  protected val uuidGenerator = new UUIDGenerator

  // APIs
  protected val restClient = new AkkaRestClient()
  protected val lisaApi = new LisaApi(restClient)

  $if(setupDB.truthy)$
  // DBs
  protected val investmentOrderDB = new InvestmentOrderDB

  // DAOs
  protected val investmentOrderDao =
    new InvestmentOrderDao(investmentOrderDB, uuidGenerator)
  $endif$

  // Services
  protected val investmentOrderService = new InvestmentOrderService(
    $if(setupDB.truthy)$investmentOrderDao$endif$
  )

  $if(useKafka.truthy)$
  // Kafka
  protected val investmentOrderEventHandler: InvestmentOrderEventHandler = new InvestmentOrderEventHandler(investmentOrderService, new InvestmentOrderEventMapper())
  protected val investmentOrderApp = new KafkaStreamsApp(InvestmentOrderStreamTopology.create(Config.Kafka.Enabled, investmentOrderEventHandler), Config.Kafka.Streams.InvestmentOrderProperties)
  $endif$
}
