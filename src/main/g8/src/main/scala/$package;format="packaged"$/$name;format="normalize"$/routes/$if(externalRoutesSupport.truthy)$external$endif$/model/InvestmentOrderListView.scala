package $package$.$name;format="normalize"$.routes.external.model

case class InvestmentOrderListView(items: List[InvestmentOrderView])
