package $package$.$name;format="normalize"$

import $package$.commons.akka.http.AkkaHttpService
  
import $package$.commons.kafka.streams.KafkaStreamsApp
import $package$.commons.rest.routes.DefaultRoutesFactory
import $package$.commons.rest.service.ServiceFactory
import $package$.flyway.DatabaseMigrations
import com.typesafe.scalalogging.StrictLogging
import org.squeryl.adapters.MySQLInnoDBAdapter
import org.squeryl.{Session, SessionFactory}

object $name;format="Camel"$ extends AkkaHttpService(BuildInfo.name) $if(setupDB.truthy)$ with DatabaseMigrations $endif$ with StrictLogging with Dependencies {

  def main(args: Array[String]): Unit = runService(Config.toString) {
    $if(setupDB.truthy)$
    initializeJdbcSession()
    migrateDatabase(Config.DataSource)
    $endif$
    val service = ServiceFactory(Config.Host, Config.Port, BuildInfo)

    val defaultRoutes = new DefaultRoutesFactory(service, Config.EnableMetricsCollection).defaultRoutes
    val routes = new Routes(defaultRoutes)
    $if(useKafka.truthy)$
    if (Config.Kafka.Enabled) startKafkaStreamsApplications()
    $endif$

    service.start(routes = routes.routes, enableMetricsCollection = Config.EnableMetricsCollection)
  }

  $if(setupDB.truthy)$
  private def initializeJdbcSession(): Unit =
    SessionFactory.concreteFactory = Some(() => Session.create(() => Config.DataSource.getConnection, new MySQLInnoDBAdapter))
  $endif$
  $if(useKafka.truthy)$
  private def startKafkaStreamsApplications(): Unit = {
    lazy val kafkaStreamApps: Seq[KafkaStreamsApp] = Seq(investmentOrderApp)
    kafkaStreamApps.foreach(_.start())
  }
  $endif$
}
