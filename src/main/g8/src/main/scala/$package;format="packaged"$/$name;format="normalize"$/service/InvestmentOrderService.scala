package $package$.$name;format="normalize"$.service

import $package$.$name;format="normalize"$.dal.InvestmentOrderDao
import $package$.$name;format="normalize"$.model._
import com.typesafe.scalalogging.LazyLogging

import java.time.LocalDate
import scala.concurrent.Future

$if(useBackgroundJobs.truthy)$
object InvestmentOrderService {
  val InvestmentOrdersJobName = "processInvestmentOrders"
}
$endif$
class InvestmentOrderService(investmentOrderDao: InvestmentOrderDao) extends LazyLogging {

  def createInvestmentOrders(createInvestmentOrder: CreateInvestmentOrder): Future[InvestmentOrder] =
    investmentOrderDao.createInvestmentOrder(createInvestmentOrder)

  def getInvestmentOrders(investmentOrderFilter: InvestmentOrderFilter): Future[List[InvestmentOrder]] =
    investmentOrderDao.getInvestmentOrders(investmentOrderFilter)

  $if(useBackgroundJobs.truthy)$
  def processInvestmentOrders(date: LocalDate): Future[LocalDate] = Future.successful(date)
  $endif$

  def deactivateInvestmentOrder(userId: UserId, investmentOrderId: InvestmentOrderId): Future[Int] =
    investmentOrderDao.deactivateInvestmentOrder(userId, investmentOrderId)

}
