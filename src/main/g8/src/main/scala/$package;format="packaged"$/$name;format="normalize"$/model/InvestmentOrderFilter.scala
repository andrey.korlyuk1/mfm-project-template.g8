package $package$.$name;format="normalize"$.model

case class InvestmentOrderFilter(userId: Option[UserId], active: Option[Boolean])
