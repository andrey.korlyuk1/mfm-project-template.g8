package $package$.$name;format="normalize"$.routes.external.model

case class CreateInvestmentOrderRequest(active: Boolean)
