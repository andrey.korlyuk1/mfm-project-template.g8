package $package$.$name;format="normalize"$.dal.infra.rest.lisa

import akka.http.scaladsl.model.StatusCodes
import $package$.$name;format="normalize"$.Config
import $package$.$name;format="normalize"$.dal.infra.rest.lisa.LisaApi.Endpoints
import $package$.$name;format="normalize"$.dal.infra.rest.lisa.model.LedgerPortfolioContract
import $package$.rest.{BaseRestClient, Response}
import com.typesafe.scalalogging.LazyLogging
import spray.json._

import scala.concurrent.{ExecutionContext, Future}

object LisaApi {
  object Endpoints {
    def portfolio(portfolioId: Long) = s"portfolios/\$portfolioId"
  }

  object Errors {
    case class NotEnoughAvailableCashError(error: String) extends RuntimeException(s"\$error")
  }
}

class LisaApi(val restClient: BaseRestClient[JsValue, Response])(implicit
    executionContext: ExecutionContext
) extends LisaProtocol
    with LazyLogging {

  lazy val lisaUrl: String = Config.LisaUrl

  def getPortfolio(portfolioId: Long): Future[Option[LedgerPortfolioContract]] =
    restClient.get(lisaUrl)(Endpoints.portfolio(portfolioId)).map {
      case Response(StatusCodes.OK.intValue, body, _) =>
        Some(body.parseJson.convertTo[LedgerPortfolioContract])
      case Response(StatusCodes.NotFound.intValue, _, _) => None
      case Response(statusCode, body, _) =>
        val errorMessage =
          s"An error occurred while getting the portfolio from Lisa with id \$portfolioId with status code \$statusCode and body \$body."
        logger.error(errorMessage)
        throw new IllegalStateException(errorMessage)
    }
}
