package $package$.$name;format="normalize"$.event.consumer

import $package$.commons.kafka.Topic
import $package$.commons.kafka.streams.{EmptyKafkaStreamsTopology, KafkaStreamsTopology}
import $package$.commons.kafka.streams.event.{Event, EventHandled, EventHandler, StreamsTopologyBranchedHandler}
import $package$.events.InvestmentOrderEvent
import $package$.events.InvestmentOrderEvent.EventType
import $package$.$name;format="normalize"$.Config
import $package$.$name;format="normalize"$.model._
import $package$.$name;format="normalize"$.service.InvestmentOrderService
import com.typesafe.scalalogging.StrictLogging

import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

object InvestmentOrderStreamTopology {
  def create(enabled: Boolean, eventHandler: InvestmentOrderEventHandler): KafkaStreamsTopology =
    if (enabled) {
      new InvestmentOrderStreamTopology(Config.Kafka.Topics.investmentOrdersTopic, eventHandler)
    } else {
      EmptyKafkaStreamsTopology
    }
}

class InvestmentOrderStreamTopology private (
    protected override val topic: Topic[String, InvestmentOrderEvent],
    protected override val eventHandler: InvestmentOrderEventHandler
) extends StreamsTopologyBranchedHandler[String, InvestmentOrderEvent] {
  protected override val topologyDescriptionPrefix = "investment-order"
}

class InvestmentOrderEventHandler(investmentOrderService: InvestmentOrderService, investmentOrderEventMapper: InvestmentOrderEventMapper)(implicit
    executionContext: ExecutionContext
) extends EventHandler[InvestmentOrderEvent]
    with StrictLogging {

  def handleEvent(event: InvestmentOrderEvent): Future[EventHandled[InvestmentOrderEvent]] = {
    logger.info(s"Processing event '\${event.eventType}:\${event.id}' - details: '\$event'")
    investmentOrderEventMapper.from(event) match {
      case Success(Some(domainEvent)) => processDomainEvent(domainEvent, event)
      case Success(None)              => Future(Right(Event.Ignored(event)))
      case Failure(error)             => Future(Left(Event.Failed(event, error)))
    }
  }

  private def processDomainEvent(
      domainEvent: InvestmentOrderDomainEvent,
      originalEvent: InvestmentOrderEvent
  ): Future[EventHandled[InvestmentOrderEvent]] =
    domainEvent match {

      case event: DeactivateInvestmentOrderDomainEvent =>
        investmentOrderService
          .deactivateInvestmentOrder(event.userId, event.investmentOrderId)
          .map(_ => Right(Event.Succeed(originalEvent)))
          .recover { case error =>
            Left(Event.Failed(originalEvent, error))
          }

      case _ =>
        logger.info(s"Ignoring event because not of the expected type \$domainEvent")
        Future.successful(Right(Event.Ignored(originalEvent)))
    }

}

class InvestmentOrderEventMapper {
  def from(protobufEvent: InvestmentOrderEvent): Try[Option[InvestmentOrderDomainEvent]] = Try {
    protobufEvent.eventType match {
      case EventType.InvestmentOrderDisabled(event) =>
        Some(
          DeactivateInvestmentOrderDomainEvent(UserId(event.userId), InvestmentOrderId(UUID.fromString(protobufEvent.id)))
        )
      case _ => None
    }
  }
}
