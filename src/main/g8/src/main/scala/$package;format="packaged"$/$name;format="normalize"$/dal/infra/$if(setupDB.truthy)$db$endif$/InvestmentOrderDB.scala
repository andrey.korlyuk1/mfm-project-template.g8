package $package$.$name;format="normalize"$.dal.infra.db

import $package$.$name;format="normalize"$.dal.infra.db.TypeMode._
import $package$.$name;format="normalize"$.dal.infra.db.model.InvestmentOrderEntity
import $package$.$name;format="normalize"$.model.{InvestmentOrderFilter, InvestmentOrderId, UserId}
import org.squeryl._
import org.squeryl.dsl.ast.{LogicalBoolean, TrueLogicalBoolean}

import java.time.{Clock, LocalDateTime}

class InvestmentOrderDB(implicit clock: Clock) extends StringKeyedModelDao[InvestmentOrderEntity] {

  override def table: Table[InvestmentOrderEntity] = DB.investmentOrder

  def getList(filter: InvestmentOrderFilter): List[InvestmentOrderEntity] =
    getList { investmentOrderEntity =>
      filtered(filter, investmentOrderEntity)
    }

  def deactivate(userId: UserId, investmentOrderId: InvestmentOrderId): Int =
    inTransaction(
      table.update { investmentOrderEntity: InvestmentOrderEntity =>
        where(investmentOrderEntity.userId === userId.value and investmentOrderEntity.id === investmentOrderId.value.toString)
          .set(investmentOrderEntity.active := false, investmentOrderEntity.updatedAt := LocalDateTime.now(clock))
      }
    )

  private def filtered(filter: InvestmentOrderFilter, investmentOrderEntity: InvestmentOrderEntity): LogicalBoolean = {

    val customFilters: List[Option[LogicalBoolean]] = List(
      filter.userId.map(_.value === investmentOrderEntity.userId),
      filter.active.map(_ === investmentOrderEntity.active)
    )

    customFilters.flatten.foldLeft[LogicalBoolean](TrueLogicalBoolean)((acc, condition) => acc and condition)
  }

}
