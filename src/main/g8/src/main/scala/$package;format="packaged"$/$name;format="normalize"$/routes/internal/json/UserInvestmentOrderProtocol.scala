package $package$.$name;format="normalize"$.routes.internal.json

import $package$.commons.http.problem.model.ProblemDetailJsonProtocol
import $package$.$name;format="normalize"$.routes.internal.model.{InvestmentOrderListView, InvestmentOrderView}
import $package$.json.spray.CommonJsonProtocol
import spray.json.DefaultJsonProtocol

trait UserInvestmentOrderProtocol extends DefaultJsonProtocol with CommonJsonProtocol with ProblemDetailJsonProtocol {

  implicit val investmentOrderViewFormat = jsonFormat2(InvestmentOrderView)
  implicit val investmentOrderListViewFormat = jsonFormat1(InvestmentOrderListView)

}
