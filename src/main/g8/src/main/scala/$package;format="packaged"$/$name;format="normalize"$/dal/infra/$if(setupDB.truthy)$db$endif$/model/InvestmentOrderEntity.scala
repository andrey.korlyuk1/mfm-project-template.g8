package $package$.$name;format="normalize"$.dal.infra.db.model

import java.time.LocalDateTime

import $package$.$name;format="normalize"$.model.CreateInvestmentOrder
import $package$.util.UUIDGenerator
import org.squeryl.KeyedEntity

case class InvestmentOrderEntity(id: String, userId: Long, active: Boolean, createdAt: LocalDateTime, updatedAt: LocalDateTime)
    extends KeyedEntity[String] {}

object InvestmentOrderEntity {

  def build(model: CreateInvestmentOrder, uuidGenerator: UUIDGenerator, currentTime: LocalDateTime): InvestmentOrderEntity =
    InvestmentOrderEntity(
      id = uuidGenerator.randomUUID().toString,
      userId = model.userId.value,
      active = true,
      createdAt = currentTime,
      updatedAt = currentTime
    )

}
