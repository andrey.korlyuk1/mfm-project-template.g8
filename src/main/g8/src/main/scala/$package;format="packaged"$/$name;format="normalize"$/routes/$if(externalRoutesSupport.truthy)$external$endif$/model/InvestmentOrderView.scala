package $package$.$name;format="normalize"$.routes.external.model

import java.util.UUID

case class InvestmentOrderView(id: UUID, active: Boolean)
