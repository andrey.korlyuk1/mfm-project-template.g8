package $package$.$name;format="normalize"$.routes.handler

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes.{InternalServerError, NotFound}
import akka.http.scaladsl.server.ExceptionHandler
import akka.http.scaladsl.server.directives.BasicDirectives.extractUri
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import $package$.commons.akka.http.exception.NotFoundException
import $package$.commons.http.problem.model.{ProblemDetail, ProblemDetailJsonProtocol}
import com.typesafe.scalalogging.Logger

trait CustomExceptionHandler extends ProblemDetailJsonProtocol with SprayJsonSupport {

  protected def logger: Logger

  def customExceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case _: NotFoundException =>
        extractUri { uri =>
          logger.warn(s"[\$uri] Not found")
          complete(NotFound -> ProblemDetail.notFoundProblemDetail)
        }
      case e: Throwable =>
        extractUri { uri =>
          logger.error(s"[\$uri] Unexpected Internal Server Error: \${e.getMessage}", e)
          complete(InternalServerError -> ProblemDetail.internalServerErrorProblemDetail)
        }
    }
}
