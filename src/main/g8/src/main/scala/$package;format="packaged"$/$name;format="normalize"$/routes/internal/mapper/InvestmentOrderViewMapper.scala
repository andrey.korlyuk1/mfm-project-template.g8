package $package$.$name;format="normalize"$.routes.internal.mapper

import $package$.$name;format="normalize"$.model.InvestmentOrder
import $package$.$name;format="normalize"$.routes.internal.model.InvestmentOrderView

object InvestmentOrderViewMapper {
  def from(model: InvestmentOrder): InvestmentOrderView =
    InvestmentOrderView(
      id = model.id.value,
      active = model.active
    )

}
