package $package$.$name;format="normalize"$.routes.external

import akka.http.scaladsl.server.Directives.pathPrefix
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

trait Routes extends InvestmentOrdersRoutes {

  val externalRoutes: Route = pathPrefix("external" / "v0") {
    authorised { _ =>
      investmentOrdersRoutes
    }
  }

}
