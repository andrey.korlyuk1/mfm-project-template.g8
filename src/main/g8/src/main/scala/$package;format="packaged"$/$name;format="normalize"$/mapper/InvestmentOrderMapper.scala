package $package$.$name;format="normalize"$.mapper

import $package$.$name;format="normalize"$.dal.infra.db.model.InvestmentOrderEntity
import $package$.$name;format="normalize"$.model
import $package$.$name;format="normalize"$.model.{InvestmentOrder, InvestmentOrderId}

import java.util.UUID

object InvestmentOrderMapper {

  def from(entity: InvestmentOrderEntity): InvestmentOrder =
    InvestmentOrder(
      id = InvestmentOrderId(UUID.fromString(entity.id)),
      userId = model.UserId(entity.userId),
      active = entity.active
    )
}
