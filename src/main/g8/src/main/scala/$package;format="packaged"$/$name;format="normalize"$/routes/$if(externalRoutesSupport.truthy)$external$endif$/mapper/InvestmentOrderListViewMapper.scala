package $package$.$name;format="normalize"$.routes.external.mapper

import $package$.$name;format="normalize"$.model.InvestmentOrder
import $package$.$name;format="normalize"$.routes.external.model.InvestmentOrderListView

object InvestmentOrderListViewMapper {
  def from(modelList: List[InvestmentOrder]): InvestmentOrderListView =
    InvestmentOrderListView(modelList.map(InvestmentOrderViewMapper.from))

}
