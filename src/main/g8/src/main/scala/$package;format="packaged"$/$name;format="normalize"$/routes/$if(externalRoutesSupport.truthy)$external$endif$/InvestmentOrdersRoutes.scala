package $package$.$name;format="normalize"$.routes.external

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes.Created
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import $package$.commons.akka.http.directives.LoggingDirectives
import $package$.commons.auth.akka.jwt.JwtDirectives
import $package$.$name;format="normalize"$.Dependencies
import $package$.$name;format="normalize"$.model.{InvestmentOrderFilter, UserId}
import $package$.$name;format="normalize"$.routes.external.json.InvestmentOrderProtocol
import $package$.$name;format="normalize"$.routes.external.mapper.{CreateInvestmentOrderMapper, InvestmentOrderListViewMapper, InvestmentOrderViewMapper}
import $package$.$name;format="normalize"$.routes.external.model.{CreateInvestmentOrderRequest, InvestmentOrderListView, InvestmentOrderView}
import io.swagger.annotations._

import javax.ws.rs.Path

@Api(value = "external", produces = "application/json", consumes = "application/json")
@Path("/")
trait InvestmentOrdersRoutes extends Dependencies with LoggingDirectives with JwtDirectives with SprayJsonSupport with InvestmentOrderProtocol {

  @ApiOperation(
    value = "create investment order",
    notes = "create investment order",
    httpMethod = "POST",
    code = 201
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "CreateInvestmentOrderRequest",
        value = "create investment order request",
        required = true,
        dataTypeClass = classOf[CreateInvestmentOrderRequest],
        paramType = "body"
      )
    )
  )
  @ApiResponses(
    Array(
      new ApiResponse(code = 201, message = "investment order created", response = classOf[InvestmentOrderView]),
      new ApiResponse(code = 400, message = "Bad request"),
      new ApiResponse(code = 401, message = "Unauthorized"),
      new ApiResponse(code = 500, message = "Internal server error")
    )
  )
  @Path("investment-orders")
  def createInvestmentOrders: Route =
    path("investment-orders")(
      postAndLog[CreateInvestmentOrderRequest] { createInvestmentOrderRequest: CreateInvestmentOrderRequest =>
        authorisedWithPermission(Permissions.createInvestmentOrder) { jwt =>
          val request = CreateInvestmentOrderMapper.from(createInvestmentOrderRequest, UserId(jwt.userInformation.userId))
          complete(Created -> investmentOrderService.createInvestmentOrders(request).map(InvestmentOrderViewMapper.from))
        }
      }
    )

  @ApiOperation(
    value = "get investment orders",
    notes = "get investment orders",
    httpMethod = "GET",
    code = 200
  )
  @ApiImplicitParams(
    Array(
      new ApiImplicitParam(
        name = "active",
        value = "list orders with this active flag only",
        required = false,
        paramType = "query"
      )
    )
  )
  @ApiResponses(
    Array(
      new ApiResponse(code = 200, message = "investment order list", response = classOf[InvestmentOrderListView]),
      new ApiResponse(code = 401, message = "Unauthorized"),
      new ApiResponse(code = 500, message = "Internal server error")
    )
  )
  def getInvestmentOrders: Route =
    path("investment-orders") {
      getAndLog {
        parameters("active".as[Boolean].?) { isActive =>
          authorisedWithPermission(Permissions.readInvestmentOrders) { jwt =>
            complete(
              investmentOrderService
                .getInvestmentOrders(InvestmentOrderFilter(Some(UserId(jwt.userInformation.userId)), isActive))
                .map(InvestmentOrderListViewMapper.from)
            )
          }
        }
      }
    }
  def investmentOrdersRoutes: Route =
    createInvestmentOrders ~ getInvestmentOrders
}
