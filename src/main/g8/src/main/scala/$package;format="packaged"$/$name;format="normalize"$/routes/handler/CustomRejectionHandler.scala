package $package$.$name;format="normalize"$.routes.handler

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.BasicDirectives._
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import $package$.commons.http.problem.model.{ProblemDetail, ProblemDetailJsonProtocol}
import com.typesafe.scalalogging.Logger
import spray.json.{JsObject, JsString, RootJsonWriter}

object CustomRejectionHandler {

  class ExtendedProblemDetail(override val `type`: String, override val title: String, val detail: String) extends ProblemDetail

  case class MethodNotAllowedProblemDetail(allowedMethods: Seq[String])
      extends ExtendedProblemDetail(
        s"\${ProblemDetail.typePrefix}/method-not-allowed",
        "Method not allowed",
        s"Supported methods: \${allowedMethods.mkString(", ")}"
      )

  case class BadRequestProblemDetail(override val title: String, override val detail: String)
      extends ExtendedProblemDetail(
        s"\${ProblemDetail.typePrefix}/bad-request",
        title,
        detail
      )

  implicit def problemDetailWriter[T <: ExtendedProblemDetail]: RootJsonWriter[T] = new RootJsonWriter[T] {
    def write(obj: T) =
      JsObject(
        Map(
          "type" -> JsString(obj.`type`),
          "title" -> JsString(obj.title),
          "detail" -> JsString(obj.detail)
        )
      )
  }
}

trait CustomRejectionHandler extends ProblemDetailJsonProtocol with SprayJsonSupport {

  protected def logger: Logger

  def customRejectionHandler: RejectionHandler =
    RejectionHandler
      .newBuilder()
      .handle { case MalformedRequestContentRejection(message, _) =>
        extractUri { uri =>
          logger.warn(s"[\$uri] Bad request: \$message")
          val title = "The request is malformed"
          val details = message
          complete(StatusCodes.BadRequest -> CustomRejectionHandler.BadRequestProblemDetail(title, details))
        }
      }
      .handle { case MalformedQueryParamRejection(parameter, message, _) =>
        extractUri { uri =>
          logger.warn(s"[\$uri] Bad request: \$message")
          val title = "The query parameters on the request are malformed"
          val details = s"\$parameter not valid"
          complete(StatusCodes.BadRequest -> CustomRejectionHandler.BadRequestProblemDetail(title, details))
        }
      }
      .handleAll[MethodRejection] { methodRejections =>
        extractUri { uri =>
          logger.warn(s"[\$uri] Method not allowed")
          val supportedMethods = methodRejections.map(_.supported.name)
          complete(StatusCodes.MethodNotAllowed -> CustomRejectionHandler.MethodNotAllowedProblemDetail(supportedMethods))
        }
      }
      .handle { case UnsupportedRequestContentTypeRejection(supported) =>
        extractUri { uri =>
          logger.warn(s"[\$uri] Bad request: unsupported content type.")
          val title = "Unsupported content type"
          val details = s"Supported content types: \${supported.mkString(",")}"
          complete(StatusCodes.BadRequest -> CustomRejectionHandler.BadRequestProblemDetail(title, details))
        }
      }
      .handleNotFound {
        extractUri { uri =>
          logger.warn(s"[\$uri] Not found")
          complete(StatusCodes.NotFound -> ProblemDetail.notFoundProblemDetail)
        }
      }
      .result()
}
