package $package$.investment.dal.infra.rest.lisa.model

import $package$.investment.model.{CashFlow, LedgerPortfolioId}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class CreateCashFlowRequestContractSpec extends AnyWordSpec with Matchers {

  "CreateCashFlowRequestContract" when {

    "build" should {
      "create a new contract instance from model" in {
        val model = CashFlow(
          ledgerPortfolioId = LedgerPortfolioId(10L),
          amount = 1,
          reason = "reason"
        )

        val expectedContract = CreateCashFlowRequestContract(
          cashflow = 1,
          reason = "reason"
        )

        CreateCashFlowRequestContract.build(model) shouldBe expectedContract
      }
    }

  }
}
