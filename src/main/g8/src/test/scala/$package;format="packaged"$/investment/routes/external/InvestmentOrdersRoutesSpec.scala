package $package$.investment.routes.external

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.Route.seal
import akka.http.scaladsl.server.directives.{BasicDirectives, RouteDirectives}
import $package$.commons.auth.jwt.{Country, JsonWebToken, LegalEntity, LegalEntityId, Partner, UserInformation}
import $package$.investment.model.{InvestmentOrderFilter, UserId}
import $package$.investment.routes.external.fixture.InvestmentOrderFixture
import $package$.investment.routes.external.json.InvestmentOrderProtocol
import $package$.investment.routes.external.model.{InvestmentOrderListView, InvestmentOrderView}
import $package$.investment.routes.external.validation.CreateInvestmentOrderRequestValidator
import $package$.investment.service.InvestmentOrderService
import $package$.investment.testing.RouteSpec
import $package$.investment.validation.ValidationError
import org.scalatest.BeforeAndAfterEach
import $package$.investment.{Routes => GlobalRoutes}
import org.mockito.scalatest.IdiomaticMockito

import scala.concurrent.Future

class InvestmentOrdersRoutesSpec
    extends RouteSpec
    with BeforeAndAfterEach
    with InvestmentOrderProtocol
    with InvestmentOrderFixture
    with IdiomaticMockito {

  private val jsonWebTokenMock = mock[JsonWebToken]
  private val investmentOrderServiceMock = mock[InvestmentOrderService]
  private val createInvestmentOrderRequestValidatorMock =
    mock[CreateInvestmentOrderRequestValidator]

  class RoutesUnderTest(override protected val investmentOrderService: InvestmentOrderService = investmentOrderServiceMock,
                        override val createInvestmentOrderRequestValidator: CreateInvestmentOrderRequestValidator =
                          createInvestmentOrderRequestValidatorMock,
                        override val authorised: Directive1[JsonWebToken] = BasicDirectives.provide(jsonWebTokenMock))
      extends GlobalRoutes(RouteDirectives.reject)

  class RoutesUnderTestNotAuthenticated extends GlobalRoutes(RouteDirectives.reject)

  private val subject = new RoutesUnderTest()
  private val subjectNotAuthenticated = new RoutesUnderTestNotAuthenticated

  val userInformation =
    UserInformation(
      LegalEntity(
        id = LegalEntityId("moneyfarm-it"),
        partner = Partner("moneyfarm"),
        country = Country("it")
      ),
      null,
      null,
      userId.value,
      null,
      isImpersonated = false,
      permissions = Set(Permissions.createInvestmentOrder, Permissions.readInvestmentOrders)
    )

  override def beforeEach(): Unit =
    reset(investmentOrderServiceMock, createInvestmentOrderRequestValidatorMock)

  "POST /external/v0/investment-orders" should {

    jsonWebTokenMock.userInformation returns userInformation

    "return 201 if the investment order is created successfully" in {

      createInvestmentOrderRequestValidatorMock.validate((createInvestmentOrderRequest, userId.value)) returns Future.successful(List.empty)

      investmentOrderServiceMock.createInvestmentOrders(createInvestmentOrder) returns Future.successful(investmentOrder)

      Post(s"/external/v0/investment-orders", createInvestmentOrderRequest) ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.Created
        responseAs[InvestmentOrderView] shouldBe investmentOrderView
      }
    }

    "return 400 if createInvestmentOrderRequestValidator fails validation" in {

      val expectedError = new ValidationError {
        override def error: String = "error"
        override def field: String = "field"
      }
      createInvestmentOrderRequestValidatorMock.validate((createInvestmentOrderRequest, userId.value)) returns Future.successful(List(expectedError))

      Post(s"/external/v0/investment-orders", createInvestmentOrderRequest) ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }

    "return 401 Unauthorized to unauthenticated calls" in {
      Post(s"/external/v0/investment-orders", createInvestmentOrderRequest) ~> seal(subjectNotAuthenticated.routes) ~> check {
        status shouldBe StatusCodes.Unauthorized
      }
    }

    "return 500 if investmentOrderService throw a RuntimeException" in {

      createInvestmentOrderRequestValidatorMock.validate((createInvestmentOrderRequest, userId.value)) returns Future.successful(List.empty)

      investmentOrderServiceMock.createInvestmentOrders(createInvestmentOrder) returns Future.failed(new RuntimeException)

      Post(s"/external/v0/investment-orders", createInvestmentOrderRequest) ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.InternalServerError
      }
    }

    "return 500 if createInvestmentOrderRequestValidator throw a RuntimeException" in {

      createInvestmentOrderRequestValidatorMock.validate((createInvestmentOrderRequest, userId.value)) throws new RuntimeException

      Post(s"/external/v0/investment-orders", createInvestmentOrderRequest) ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.InternalServerError
      }
    }

    "return 401 if  user does not have right permissions" in {
      jsonWebTokenMock.userInformation returns userInformation.copy(permissions = Set())

      Post(s"/external/v0/investment-orders", createInvestmentOrderRequest) ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.Unauthorized
      }
    }
  }
  "GET /external/v0/investment-orders" should {

    "return 200 if order list is returned" in {
      jsonWebTokenMock.userInformation returns userInformation
      investmentOrderServiceMock.getInvestmentOrders(*) returns Future.successful(List(investmentOrder))
      Get(s"/external/v0/investment-orders") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.OK
        responseAs[InvestmentOrderListView] shouldBe investmentOrderListView
        investmentOrderServiceMock.getInvestmentOrders(InvestmentOrderFilter(Some(UserId(userInformation.userId)), None, None)) wasCalled (once)
      }
    }

    "return 200" when {
      val portfolioId = "a-portfolio-id"

      "active query param is passed" in {
        val expectedFilter = InvestmentOrderFilter(Some(userId), active = Some(true), None)
        jsonWebTokenMock.userInformation returns userInformation
        investmentOrderServiceMock.getInvestmentOrders(expectedFilter) returns Future.successful(List(investmentOrder))
        Get(s"/external/v0/investment-orders?active=true") ~> seal(subject.routes) ~> check {
          status shouldBe StatusCodes.OK
          responseAs[InvestmentOrderListView] shouldBe investmentOrderListView
          investmentOrderServiceMock.getInvestmentOrders(expectedFilter) wasCalled (once)
        }
      }

      "portfolioId query param is passed" in {
        val expectedFilter = InvestmentOrderFilter(Some(userId), active = None, Some(portfolioId))
        jsonWebTokenMock.userInformation returns userInformation
        investmentOrderServiceMock.getInvestmentOrders(expectedFilter) returns Future.successful(List(investmentOrder))
        Get(s"/external/v0/investment-orders?portfolioId=\$portfolioId") ~> seal(subject.routes) ~> check {
          status shouldBe StatusCodes.OK
          responseAs[InvestmentOrderListView] shouldBe investmentOrderListView
          investmentOrderServiceMock.getInvestmentOrders(expectedFilter) wasCalled (once)
        }
      }
      "all query param are passed" in {
        val expectedFilter = InvestmentOrderFilter(Some(userId), active = Some(true), Some(portfolioId))
        jsonWebTokenMock.userInformation returns userInformation
        investmentOrderServiceMock.getInvestmentOrders(expectedFilter) returns Future.successful(List(investmentOrder))
        Get(s"/external/v0/investment-orders?active=true&portfolioId=\$portfolioId") ~> seal(subject.routes) ~> check {
          status shouldBe StatusCodes.OK
          responseAs[InvestmentOrderListView] shouldBe investmentOrderListView
          investmentOrderServiceMock.getInvestmentOrders(expectedFilter) wasCalled (once)
        }
      }
    }

    "return 500 if underlying service fails" in {

      jsonWebTokenMock.userInformation returns userInformation
      investmentOrderServiceMock.getInvestmentOrders(*) throws new RuntimeException

      Post(s"/external/v0/investment-orders", createInvestmentOrderRequest) ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.InternalServerError
      }
    }

    "return 401 Unauthorized to unauthenticated calls" in {
      Get(s"/external/v0/investment-orders") ~> seal(subjectNotAuthenticated.routes) ~> check {
        status shouldBe StatusCodes.Unauthorized
      }
    }
  }
}
