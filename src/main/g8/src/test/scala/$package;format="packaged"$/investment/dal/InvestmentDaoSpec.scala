package $package$.investment.dal

import java.time.LocalDate
import java.util.UUID

import $package$.investment.dal.infra.db.InvestmentDB
import $package$.investment.dal.infra.db.model.InvestmentEntity
import $package$.investment.fixture.InvestmentOrderFixture
import $package$.investment.model.CashFlowId
import $package$.investment.testing.{CustomMatchers, FixedClock}
import $package$.util.UUIDGenerator
import org.mockito.scalatest.MockitoSugar
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.ExecutionContext.Implicits.global

class InvestmentDaoSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with FixedClock
    with InvestmentOrderFixture
    with ScalaFutures
    with CustomMatchers {

  class Fixture {

    val investmentDBMock = mock[InvestmentDB]
    val uuidGeneratorMock = mock[UUIDGenerator]

    val subject = new InvestmentDao(investmentDBMock, uuidGeneratorMock)

  }

  "InvestmentDao" when {

    val dueDate = LocalDate.now

    val cashFlowId = CashFlowId(100)
    val investmentId = UUID.randomUUID
    val investmentEntity = InvestmentEntity(
      id = investmentId.toString,
      investmentOrderId = investmentOrder.id.value.toString,
      amount = investmentOrder.amount,
      dueDate = dueDate,
      executionDate = fixedClock.instant,
      virtualCashFlowId = cashFlowId.id,
      destination = investmentOrder.destination.value.toString,
      reason = None,
      createdAt = fixedClock.instant,
      updatedAt = fixedClock.instant
    )

    "saveInvestment" should {

      "insert a new investment in the DB" in new Fixture {

        when(uuidGeneratorMock.randomUUID()).thenReturn(investmentId)
        when(investmentDBMock.insert(argThat(theSameAs(investmentEntity)))).thenReturn(investmentEntity)

        subject.saveInvestment(investmentOrder, cashFlowId, dueDate).futureValue shouldBe (())

      }

      "fail if the DB call fails" in new Fixture {

        val error = new RuntimeException("test")

        when(uuidGeneratorMock.randomUUID()).thenReturn(investmentId)
        when(investmentDBMock.insert(argThat(theSameAs(investmentEntity)))).thenThrow(error)

        subject.saveInvestment(investmentOrder, cashFlowId, dueDate).failed.futureValue shouldBe error

      }

    }

  }

}
