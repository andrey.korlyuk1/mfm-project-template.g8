package $package$.investment.dal.infra.db.model

import java.time.LocalDate
import java.util.UUID

import $package$.investment.fixture.InvestmentOrderFixture
import $package$.investment.model.CashFlowId
import $package$.investment.testing.CustomMatchers
import $package$.util.UUIDGenerator
import org.mockito.scalatest.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class InvestmentEntitySpec extends AnyWordSpec with Matchers with MockitoSugar with InvestmentOrderFixture with CustomMatchers {

  class Fixture {
    val uuidGeneratorMock = mock[UUIDGenerator]
  }

  val cashFlowId = CashFlowId(100)
  val investmentId = UUID.randomUUID
  val dueDate = LocalDate.now
  val investmentEntity = InvestmentEntity(
    id = investmentId.toString,
    investmentOrderId = investmentOrder.id.value.toString,
    amount = investmentOrder.amount,
    dueDate = dueDate,
    executionDate = fixedClock.instant,
    virtualCashFlowId = cashFlowId.id,
    destination = investmentOrder.destination.value.toString,
    reason = None,
    createdAt = fixedClock.instant,
    updatedAt = fixedClock.instant
  )

  "InvestmentEntity" when {

    "build" should {

      "create an InvestmentEntity" in new Fixture {

        when(uuidGeneratorMock.randomUUID()).thenReturn(investmentId)

        InvestmentEntity.build(investmentOrder, cashFlowId, uuidGeneratorMock, fixedClock.instant, dueDate) shouldBe theSameAs(investmentEntity)

      }

    }

  }

}
