package $package$.investment.dal.infra.rest.portfolio.mapper

import $package$.investment.dal.infra.rest.portfolio.model.FundingRequestValidationContract
import $package$.investment.dal.infra.rest.portfolio.model.enums.ValidateForFundingFrequency
import $package$.investment.dal.infra.rest.portfolio.model.enums.ValidateForFundingMethodType.WireTransfer
import $package$.investment.dal.infra.rest.portfolio.model.enums.ValidateForFundingSourceType.AvailableCash
import $package$.investment.dal.infra.rest.portfolio.model.enums.mapper.ValidateForFundingFrequencyMapper
import $package$.investment.model.FrequencyType
import $package$.investment.routes.external.model.{CreateInvestmentOrderRequest, InvestmentOrderFrequencyView}
import org.mockito.scalatest.IdiomaticMockito
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.util.UUID

class FundingRequestValidationContractMapperSpec extends AnyWordSpec with Matchers with IdiomaticMockito {
  "ValidateForFundingRequestContractMapper" when {
    "from(:CreateInvestmentOrderRequest)" should {
      val amount = BigDecimal(123)
      val destination = UUID.randomUUID
      val investmentOrderFrequency = InvestmentOrderFrequencyView(
        `type` = FrequencyType.Monthly,
        day = 5
      )
      val mappedInvestmentOrderFrequency = ValidateForFundingFrequency.Monthly
      val request: CreateInvestmentOrderRequest = CreateInvestmentOrderRequest(
        frequency = investmentOrderFrequency,
        amount = amount,
        destination = destination.toString
      )
      val mappedContract = FundingRequestValidationContract(
        amount = amount,
        sourceType = AvailableCash,
        method = WireTransfer,
        frequency = mappedInvestmentOrderFrequency
      )

      "construct the expected ValidateForFundingRequestContract" in {
        withObjectMocked[ValidateForFundingFrequencyMapper.type] {
          ValidateForFundingFrequencyMapper.from(investmentOrderFrequency.`type`) returns mappedInvestmentOrderFrequency
          FundingRequestValidationContractMapper.from(request.amount, request.frequency.`type`) shouldBe mappedContract
        }
      }
    }
  }
}
