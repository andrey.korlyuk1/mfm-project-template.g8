package $package$.investment.testing

import java.time.{Clock, LocalDateTime, ZoneOffset}

trait FixedClock {
  protected val fixedDateTime: LocalDateTime
  implicit protected def fixedClock: Clock =
    Clock.fixed(fixedDateTime.toInstant(ZoneOffset.UTC), ZoneOffset.UTC)
}
