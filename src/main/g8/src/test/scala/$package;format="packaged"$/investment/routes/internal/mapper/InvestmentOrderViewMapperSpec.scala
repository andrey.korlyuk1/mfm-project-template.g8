package $package$.investment.routes.internal.mapper

import $package$.investment.routes.internal.fixture.InvestmentOrderFixture
import $package$.investment.model.FrequencyDayNotDefined
import $package$.investment.testing.ActorSystemSpec
import org.scalatest.matchers.should.Matchers

class InvestmentOrderViewMapperSpec extends ActorSystemSpec with Matchers with InvestmentOrderFixture {

  "InvestmentOrderViewMapper" when {

    "from" should {

      "successfully map model to view" in {
        InvestmentOrderViewMapper.from(investmentOrder) shouldBe investmentOrderView
      }

      "fail with a FrequencyDayNotDefined if model has not a frequency day" in {
        a[FrequencyDayNotDefined] should be thrownBy InvestmentOrderViewMapper.from(
          investmentOrder.copy(frequency = investmentOrderFrequency.copy(day = None)))
      }

    }
  }
}
