package $package$.investment.dal

import $package$.investment.dal.infra.rest.lisa.LisaApi
import $package$.investment.dal.infra.rest.lisa.LisaApi.Errors.NotEnoughAvailableCashError
import $package$.investment.dal.infra.rest.lisa.model.{CreateCashFlowRequestContract, CreateCashFlowResponseContract}
import $package$.investment.model.{CashFlow, CashFlowId, LedgerPortfolioId}
import $package$.investment.testing.ActorSystemSpec
import org.mockito.Mockito
import org.mockito.scalatest.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Future

class CashFlowDaoSpec extends ActorSystemSpec with MockitoSugar with Matchers with BeforeAndAfterEach {

  val lisaApiMock = mock[LisaApi]

  override def beforeEach(): Unit = {
    Mockito.reset(lisaApiMock)
    super.beforeEach()
  }

  val cashFlowDao = new CashFlowDao(lisaApiMock)

  "CashFlowDao" when {

    "createCashFlow" should {
      val ledgerPortfolioId = LedgerPortfolioId(10L)
      val cashFlowId = 11L
      val cashFlow = CashFlow(
        ledgerPortfolioId = ledgerPortfolioId,
        amount = 1,
        reason = "reason"
      )
      val createCashFlowRequestContract = CreateCashFlowRequestContract(
        cashflow = 1,
        reason = "reason"
      )
      val createCashFlowResponseContract = CreateCashFlowResponseContract(
        id = cashFlowId
      )

      "successfully create a new cash flow and get the CashFlowId" in {

        val expectedResult = CashFlowId(cashFlowId)
        when(lisaApiMock.createCashFlow(ledgerPortfolioId.value, createCashFlowRequestContract))
          .thenReturn(Future.successful(createCashFlowResponseContract))

        cashFlowDao.createCashFlow(cashFlow).futureValue shouldBe expectedResult
      }

      "fail with a NotAvailableCashException if lisaApi fails because there is not enough available cash" in {

        when(lisaApiMock.createCashFlow(ledgerPortfolioId.value, createCashFlowRequestContract))
          .thenReturn(Future.failed(NotEnoughAvailableCashError("error")))

        cashFlowDao.createCashFlow(cashFlow).failed.futureValue shouldBe a[NotAvailableCashException]
      }

      "fail with a CreateCashFlowException if lisaApi fails with an IllegalStateException" in {

        when(lisaApiMock.createCashFlow(ledgerPortfolioId.value, createCashFlowRequestContract)).thenReturn(Future.failed(new IllegalStateException))

        cashFlowDao.createCashFlow(cashFlow).failed.futureValue shouldBe a[CreateCashFlowException]
      }

    }
  }
}
