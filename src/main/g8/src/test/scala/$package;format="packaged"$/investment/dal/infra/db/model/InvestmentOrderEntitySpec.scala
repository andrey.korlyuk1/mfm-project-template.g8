package $package$.investment.dal.infra.db.model

import java.time.LocalDateTime
import java.util.UUID

import $package$.investment.fixture.InvestmentOrderFixture
import $package$.investment.testing.{ActorSystemSpec, CustomMatchers}
import $package$.util.UUIDGenerator
import org.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers

class InvestmentOrderEntitySpec
    extends ActorSystemSpec
    with Matchers
    with MockitoSugar
    with BeforeAndAfterEach
    with CustomMatchers
    with InvestmentOrderFixture {

  val uuidGeneratorMock = mock[UUIDGenerator]

  override def beforeEach(): Unit = {
    reset(uuidGeneratorMock)
    super.beforeEach()
  }

  "InvestmentOrderEntity" when {

    "build" should {

      "successfully create an InvestmentOrderEntity" in {

        val investmentOrderId = "4cc75b66-95e8-4657-922b-5cf274fd5d1c"
        val expectedResult = investmentOrderEntity.copy(id = investmentOrderId)

        when(uuidGeneratorMock.randomUUID()).thenReturn(UUID.fromString(investmentOrderId))

        InvestmentOrderEntity.build(model = createInvestmentOrder, uuidGenerator = uuidGeneratorMock, currentTime = LocalDateTime.now(fixedClock)) shouldBe theSameAs(
          expectedResult)
      }

    }
  }
}
