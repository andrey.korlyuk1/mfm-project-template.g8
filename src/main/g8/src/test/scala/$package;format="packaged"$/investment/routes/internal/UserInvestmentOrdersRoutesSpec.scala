package $package$.investment.routes.internal

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route.seal
import akka.http.scaladsl.server.directives.RouteDirectives
import $package$.investment.routes.internal.fixture.InvestmentOrderFixture
import $package$.investment.routes.internal.json.UserInvestmentOrderProtocol
import $package$.investment.routes.internal.model.InvestmentOrderListView
import $package$.investment.service.InvestmentOrderService
import $package$.investment.testing.RouteSpec
import $package$.investment.{Routes => GlobalRoutes}
import org.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterEach

import scala.concurrent.Future

class UserInvestmentOrdersRoutesSpec
    extends RouteSpec
    with BeforeAndAfterEach
    with UserInvestmentOrderProtocol
    with MockitoSugar
    with InvestmentOrderFixture {

  private val investmentOrderServiceMock = mock[InvestmentOrderService]

  class RoutesUnderTest(override protected val investmentOrderService: InvestmentOrderService = investmentOrderServiceMock)
      extends GlobalRoutes(RouteDirectives.reject)

  class RoutesUnderTestNotAuthenticated extends GlobalRoutes(RouteDirectives.reject)

  private val subject = new RoutesUnderTest()

  override def beforeEach(): Unit =
    reset(investmentOrderServiceMock)

  "GET /users/{userId}/investment-orders" should {

    "return 200 with the list of all investment orders for the user" in {

      val filter = investmentOrderFilter.copy(active = None)
      val investmentOrderList = List(investmentOrder)

      when(investmentOrderServiceMock.getInvestmentOrders(filter)).thenReturn(Future.successful(investmentOrderList))

      Get(s"/users/\${userId.value}/investment-orders") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.OK
        responseAs[InvestmentOrderListView] shouldBe investmentOrderListView
      }
    }

    "return 200 with the list of active investment orders for the user" in {

      val investmentOrderList = List(investmentOrder)

      when(investmentOrderServiceMock.getInvestmentOrders(investmentOrderFilter)).thenReturn(Future.successful(investmentOrderList))

      Get(s"/users/\${userId.value}/investment-orders?active=true") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.OK
        responseAs[InvestmentOrderListView] shouldBe investmentOrderListView
      }
    }

    "return 400 for malformed query parameters" in {

      Get(s"/users/\${userId.value}/investment-orders?active=invalid") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.BadRequest
      }
    }

    "return 500 if investmentOrderService throw an exception" in {

      when(investmentOrderServiceMock.getInvestmentOrders(investmentOrderFilter)).thenReturn(Future.failed(new RuntimeException))

      Get(s"/users/\${userId.value}/investment-orders?active=false") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.InternalServerError
      }
    }

  }

  "DELETE /users/{userId}/investment-orders" should {

    "return 204 No Content if investment order has been deleted" in {

      when(investmentOrderServiceMock.deactivateInvestmentOrder(userId, investmentOrderId)).thenReturn(Future.successful(1))

      Delete(s"/users/\${userId.value}/investment-orders/\${investmentOrderId.value}") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.NoContent
      }
    }

    "return 404 Not Found if the investment order resource doesn't exists" in {

      when(investmentOrderServiceMock.deactivateInvestmentOrder(userId, investmentOrderId)).thenReturn(Future.successful(0))

      Delete(s"/users/\${userId.value}/investment-orders/\${investmentOrderId.value}") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }

    "return 500 if investmentOrderService throw an exception" in {

      when(investmentOrderServiceMock.deactivateInvestmentOrder(userId, investmentOrderId)).thenReturn(Future.failed(new RuntimeException))

      Delete(s"/users/\${userId.value}/investment-orders/\${investmentOrderId.value}") ~> seal(subject.routes) ~> check {
        status shouldBe StatusCodes.InternalServerError
      }
    }

  }
}
