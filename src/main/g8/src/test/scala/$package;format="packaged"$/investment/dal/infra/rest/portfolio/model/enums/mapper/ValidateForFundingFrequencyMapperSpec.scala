package $package$.investment.dal.infra.rest.portfolio.model.enums.mapper

import $package$.investment.dal.infra.rest.portfolio.model.enums.ValidateForFundingFrequency
import $package$.investment.model.FrequencyType
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class ValidateForFundingFrequencyMapperSpec extends AnyWordSpec with Matchers {
  "from" should {
    Seq((FrequencyType.Monthly, ValidateForFundingFrequency.Monthly)).foreach {
      case (model, contract) =>
        s"correctly map from \$model to the corresponding \$contract" in {
          ValidateForFundingFrequencyMapper.from(model) shouldBe contract
        }
    }
  }
}
