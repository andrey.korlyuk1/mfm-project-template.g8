package $package$.investment.routes.external.mapper

import $package$.commons.http.problem.model.{InvalidParameter, ProblemDetail}
import $package$.investment.routes.external.fixture.InvestmentOrderFixture
import $package$.investment.testing.ActorSystemSpec
import $package$.investment.validation.ValidationError
import org.scalatest.matchers.should.Matchers

class InvalidParametersProblemDetailMapperSpec extends ActorSystemSpec with Matchers with InvestmentOrderFixture {

  "InvalidParametersProblemDetailMapper" when {

    val validationError = new ValidationError {
      override def error: String = "error"
      override def field: String = "field"
    }

    "from" should {

      "successfully map model to response" in {
        val expectedResult = ProblemDetail.invalidParametersProblemDetail(
          List(
            InvalidParameter(
              name = "field",
              reason = "error"
            )))
        InvalidParametersProblemDetailMapper.from(List(validationError)) shouldBe expectedResult
      }

    }
  }
}
