package $package$.investment.service

import $package$.investment.dal.InvestmentOrderDao
import $package$.investment.fixture.InvestmentOrderFixture
import $package$.investment.model.PortfolioId
import $package$.investment.testing.ActorSystemSpec
import org.mockito.scalatest.IdiomaticMockito
import org.scalatest.matchers.should.Matchers

import java.util.UUID
import scala.concurrent.Future

class InvestmentOrderServiceSpec extends ActorSystemSpec with IdiomaticMockito with Matchers with InvestmentOrderFixture {

  private class Fixture {
    val investmentOrderDaoMock = mock[InvestmentOrderDao]

    val investmentOrderService = new InvestmentOrderService(investmentOrderDaoMock)
  }

  "InvestmentOrderService" when {

    "createInvestmentOrder" should {

      "successfully create a new InvestmentOrder" in new Fixture {
        investmentOrderDaoMock.createInvestmentOrder(createInvestmentOrder) returns Future.successful(investmentOrder)

        investmentOrderService.createInvestmentOrders(createInvestmentOrder).futureValue shouldBe investmentOrder
      }

      "fail if the investmentOrderDao fails" in new Fixture {
        val expectedException = new RuntimeException

        investmentOrderDaoMock.createInvestmentOrder(createInvestmentOrder) returns Future.failed(expectedException)

        investmentOrderService.createInvestmentOrders(createInvestmentOrder).failed.futureValue shouldBe expectedException

      }

    }

    "getInvestmentOrders" should {

      "return the list of investment orders that matches the filter" in new Fixture {
        val expectedResult = List(investmentOrder)
        investmentOrderDaoMock.getInvestmentOrders(investmentOrderFilter) returns Future.successful(expectedResult)

        investmentOrderService.getInvestmentOrders(investmentOrderFilter).futureValue shouldBe expectedResult
      }

      "return an empty list if no investment orders matches the filter" in new Fixture {
        investmentOrderDaoMock.getInvestmentOrders(investmentOrderFilter) returns Future.successful(List.empty)

        investmentOrderService.getInvestmentOrders(investmentOrderFilter).futureValue shouldBe List.empty
      }

      "fail if the investmentOrderDao fails" in new Fixture {
        val expectedException = new RuntimeException

        investmentOrderDaoMock.getInvestmentOrders(investmentOrderFilter) returns Future.failed(expectedException)

        investmentOrderService.getInvestmentOrders(investmentOrderFilter).failed.futureValue shouldBe expectedException

      }

    }

    "deactivateInvestmentOrder" should {

      "return 1 if find and deactivate the investment order given by userId and investmentOrderId couple" in new Fixture {
        investmentOrderDaoMock.deactivateInvestmentOrder(userId, investmentOrderId) returns Future.successful(1)

        investmentOrderService.deactivateInvestmentOrder(userId, investmentOrderId).futureValue shouldBe 1
      }

      "return 0 if can't find any investment order given by userId and investmentOrderId couple" in new Fixture {
        investmentOrderDaoMock.deactivateInvestmentOrder(userId, investmentOrderId) returns Future.successful(0)

        investmentOrderService.deactivateInvestmentOrder(userId, investmentOrderId).futureValue shouldBe 0
      }

      "fail if the investmentOrderDao fails" in new Fixture {
        val expectedException = new RuntimeException
        investmentOrderDaoMock.deactivateInvestmentOrder(userId, investmentOrderId) returns Future.failed(expectedException)

        investmentOrderService.deactivateInvestmentOrder(userId, investmentOrderId).failed.futureValue shouldBe expectedException

      }

    }

    "deactivateInvestmentOrdersByDestination" should {
      val destination = PortfolioId(UUID.randomUUID())
      "deactivate investment orders with the given destination" in new Fixture {
        val expectedResult = 2
        investmentOrderDaoMock.deactivateInvestmentOrdersByDestination(destination) returns Future.successful(expectedResult)

        investmentOrderService.deactivateInvestmentOrdersByDestination(destination).futureValue shouldBe expectedResult
      }

      "fails" when {
        "investmentOrderDao fails deactivating the investment orders" in new Fixture {
          val expectedError = new RuntimeException("TEST")
          investmentOrderDaoMock.deactivateInvestmentOrdersByDestination(destination) returns Future.failed(expectedError)

          investmentOrderService.deactivateInvestmentOrdersByDestination(destination).failed.futureValue shouldBe expectedError
        }
      }
    }
  }
}
