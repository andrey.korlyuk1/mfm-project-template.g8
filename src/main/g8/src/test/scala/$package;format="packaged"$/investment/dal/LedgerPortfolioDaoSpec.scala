package $package$.investment.dal

import $package$.investment.dal.infra.rest.lisa.LisaApi
import $package$.investment.dal.infra.rest.lisa.model.LedgerPortfolioContract
import $package$.investment.model.{LedgerPortfolio, LedgerPortfolioId}
import org.mockito.scalatest.MockitoSugar
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.{ExecutionContext, Future}

class LedgerPortfolioDaoSpec extends AnyWordSpec with Matchers with MockitoSugar with ScalaFutures {

  implicit val ec: ExecutionContext = ExecutionContext.global

  trait Fixture {
    val lisaApiMock = mock[LisaApi]
    val subject = new LedgerPortfolioDao(lisaApiMock)
  }

  val ledgerPortfolioId = LedgerPortfolioId(10)
  val ledgerPortfolio =
    LedgerPortfolio(ledgerPortfolioId = ledgerPortfolioId, parentLedgerId = None, availableCashForInvestment = 0)

  "getLedgerPortfolio" should {

    "return the ledger portfolio" in new Fixture {
      val ledgerContract = LedgerPortfolioContract(id = ledgerPortfolioId.value, parentId = None, availableCashForInvestment = 0)
      when(lisaApiMock.getPortfolio(ledgerPortfolioId.value)).thenReturn(Future.successful(Some(ledgerContract)))
      subject.getLedgerPortfolio(ledgerPortfolioId).futureValue shouldBe ledgerPortfolio
    }

    "fail with a LedgerPortfolioNotFound " when {
      "the underlying service return None" in new Fixture {
        when(lisaApiMock.getPortfolio(ledgerPortfolioId.value)).thenReturn(Future.successful(None))
        subject.getLedgerPortfolio(ledgerPortfolioId).failed.futureValue shouldBe LedgerPortfolioNotFound(ledgerPortfolioId)
      }
    }

    "fail with the underlying exception" when {
      "the underlying service fails " in new Fixture {
        val exception = new RuntimeException("TEST")
        when(lisaApiMock.getPortfolio(ledgerPortfolioId.value)).thenReturn(Future.failed(exception))
        subject.getLedgerPortfolio(ledgerPortfolioId).failed.futureValue shouldBe exception
      }
    }
  }
}
