package $package$.investment.event

import $package$.events.PortfolioEvent
import $package$.investment.event.consumer.{InvestmentOrderEventHandler, InvestmentOrderEventHandlerPolicies}
import $package$.investment.model.{DeactivateInvestmentOrderDomainEvent, Event, IgnoreCommand, PortfolioId}
import $package$.investment.service.InvestmentOrderService
import $package$.investment.testing.ActorSystemSpec
import org.mockito.scalatest.IdiomaticMockito
import org.scalatest.EitherValues
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.util.UUID
import scala.concurrent.Future

class PortfolioEventHandlerSpec extends AnyWordSpec with Matchers with IdiomaticMockito with EitherValues with ActorSystemSpec {

  private class Fixture {
    val investmentOrderServiceMock: InvestmentOrderService = mock[InvestmentOrderService]
    val subject = new InvestmentOrderEventHandler(investmentOrderServiceMock)
  }

  "PortfolioEventHandler" when {
    "handleEvent" should {
      val portfolioEvent = mock[PortfolioEvent]
      val portfolioId = PortfolioId(UUID.randomUUID())
      val deactivateInvestmentOrderCommand = DeactivateInvestmentOrderCommand(portfolioId)

      "return a Succeed event and deactivate all the investment orders for the given blocked portfolio" in new Fixture {
        withObjectMocked[InvestmentOrderEventHandlerPolicies.type] {
          val expectedResult = Event.Succeed(portfolioEvent)

          InvestmentOrderEventHandlerPolicies.from(portfolioEvent) returns deactivateInvestmentOrderCommand
          investmentOrderServiceMock.deactivateInvestmentOrdersByDestination(portfolioId) returns Future.successful(2)

          subject.handleEvent(portfolioEvent).futureValue.value shouldBe expectedResult
        }
      }

      "return a Failed event when investmentOrderService fails deactivating the investment orders" in new Fixture {
        withObjectMocked[InvestmentOrderEventHandlerPolicies.type] {
          val error = new RuntimeException("TEST")
          val expectedResult = Event.Failed(portfolioEvent, error)

          InvestmentOrderEventHandlerPolicies.from(portfolioEvent) returns deactivateInvestmentOrderCommand
          investmentOrderServiceMock.deactivateInvestmentOrdersByDestination(portfolioId) returns Future.failed(error)

          subject.handleEvent(portfolioEvent).futureValue.left.value shouldBe expectedResult
        }
      }

      "return an Ignored event and do nothing" in new Fixture {
        withObjectMocked[InvestmentOrderEventHandlerPolicies.type] {
          val expectedResult = Event.Ignored(portfolioEvent)

          InvestmentOrderEventHandlerPolicies.from(portfolioEvent) returns IgnoreCommand
          subject.handleEvent(portfolioEvent).futureValue.value shouldBe expectedResult
        }
      }
    }
  }
}
