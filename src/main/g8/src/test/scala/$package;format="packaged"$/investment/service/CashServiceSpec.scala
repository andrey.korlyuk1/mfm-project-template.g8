package $package$.investment.service

import $package$.investment.dal.LedgerPortfolioDao
import $package$.investment.model.{AvailableCash, LedgerPortfolio, LedgerPortfolioId}
import org.mockito.scalatest.MockitoSugar
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.{ExecutionContext, Future}

class CashServiceSpec extends AnyWordSpec with MockitoSugar with Matchers with ScalaFutures {

  implicit val ec: ExecutionContext = ExecutionContext.global

  trait Fixture {
    val ledgerPortfolioDaoMock = mock[LedgerPortfolioDao]
    val service = new CashService(ledgerPortfolioDaoMock)
  }

  val parentLedgerPortfolioId = LedgerPortfolioId(100)
  val ledgerPortfolioId = LedgerPortfolioId(101)
  val availableCash = AvailableCash(1000)

  "getAvailableCash" should {

    "return the available cash for a given portfolio" in new Fixture {
      val childLedger =
        LedgerPortfolio(ledgerPortfolioId = ledgerPortfolioId, parentLedgerId = Some(parentLedgerPortfolioId), availableCashForInvestment = 0)
      val parentLedger =
        LedgerPortfolio(ledgerPortfolioId = parentLedgerPortfolioId, parentLedgerId = None, availableCashForInvestment = availableCash.value)

      when(ledgerPortfolioDaoMock.getLedgerPortfolio(ledgerPortfolioId)).thenReturn(Future.successful(childLedger))
      when(ledgerPortfolioDaoMock.getLedgerPortfolio(parentLedgerPortfolioId)).thenReturn(Future.successful(parentLedger))

      service.getAvailableCash(ledgerPortfolioId).futureValue shouldBe availableCash
    }

    "fail with a LedgerPortfolioWithoutParentException" when {
      "the required portfolio do not has a parent portfolio" in new Fixture {
        val childLedger =
          LedgerPortfolio(ledgerPortfolioId = ledgerPortfolioId, parentLedgerId = None, availableCashForInvestment = 0)

        when(ledgerPortfolioDaoMock.getLedgerPortfolio(ledgerPortfolioId)).thenReturn(Future.successful(childLedger))
        service.getAvailableCash(ledgerPortfolioId).failed.futureValue shouldBe LedgerPortfolioWithoutParentException(ledgerPortfolioId)
      }
    }

    "fail with the underlying exception" when {
      "the underlying service fails" in new Fixture {
        val exception = new RuntimeException
        LedgerPortfolio(ledgerPortfolioId = ledgerPortfolioId, parentLedgerId = Some(parentLedgerPortfolioId), availableCashForInvestment = 0)

        when(ledgerPortfolioDaoMock.getLedgerPortfolio(ledgerPortfolioId)).thenReturn(Future.failed(exception))
        service.getAvailableCash(ledgerPortfolioId).failed.futureValue shouldBe exception
      }
    }
  }
}
