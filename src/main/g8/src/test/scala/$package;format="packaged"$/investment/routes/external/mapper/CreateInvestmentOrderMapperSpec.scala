package $package$.investment.routes.external.mapper

import $package$.investment.routes.external.fixture.InvestmentOrderFixture
import $package$.investment.testing.ActorSystemSpec
import org.scalatest.matchers.should.Matchers

class CreateInvestmentOrderMapperSpec extends ActorSystemSpec with Matchers with InvestmentOrderFixture {

  "CreateInvestmentOrderMapper" when {

    "from" should {

      "successfully map request to model" in {
        CreateInvestmentOrderMapper.from(createInvestmentOrderRequest, userId) shouldBe createInvestmentOrder
      }

    }
  }
}
