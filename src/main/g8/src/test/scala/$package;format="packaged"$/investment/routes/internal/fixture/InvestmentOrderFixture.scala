package $package$.investment.routes.internal.fixture

import $package$.investment.model._
import $package$.investment.routes.internal.model.{InvestmentOrderFrequencyView, InvestmentOrderListView, InvestmentOrderView}
import $package$.investment.testing.FixedClock

import java.time.LocalDateTime
import java.util.UUID

trait InvestmentOrderFixture extends FixedClock {

  override protected val fixedDateTime = LocalDateTime.now()

  val investmentOrderFrequencyView =
    InvestmentOrderFrequencyView(FrequencyType.Monthly, 3)
  val userId = UserId(11L)
  val investmentOrderId = InvestmentOrderId(UUID.fromString("6c0cea39-c8e3-4dda-8974-2bb727790866"))
  val portfolioId = PortfolioId(UUID.fromString("4cc75b66-95e8-4657-922b-5cf274fd5d1b"))

  val investmentOrderFrequency =
    InvestmentOrderFrequency(FrequencyType.Monthly, Some(3))

  val investmentOrder = InvestmentOrder(
    id = investmentOrderId,
    userId = userId,
    amount = 10,
    destination = portfolioId,
    active = true,
    frequency = investmentOrderFrequency
  )

  val investmentOrderView = InvestmentOrderView(
    id = UUID.fromString("6c0cea39-c8e3-4dda-8974-2bb727790866"),
    amount = 10,
    destination = UUID.fromString("4cc75b66-95e8-4657-922b-5cf274fd5d1b"),
    active = true,
    frequency = investmentOrderFrequencyView
  )

  val investmentOrderListView = InvestmentOrderListView(
    items = List(investmentOrderView)
  )

  val investmentOrderFilter =
    InvestmentOrderFilter(userId = Some(userId), active = Some(true), None)

}
