package $package$.investment.routes.internal.mapper

import $package$.investment.model.FrequencyDayNotDefined
import $package$.investment.routes.internal.fixture.InvestmentOrderFixture
import $package$.investment.testing.ActorSystemSpec
import org.scalatest.matchers.should.Matchers

class InvestmentOrderListViewMapperSpec extends ActorSystemSpec with Matchers with InvestmentOrderFixture {

  "InvestmentOrderListViewMapper" when {

    "from" should {

      "successfully map model to response" in {
        InvestmentOrderListViewMapper.from(List(investmentOrder)) shouldBe investmentOrderListView
      }

      "fail with a FrequencyDayNotDefined if one of the investment orders has not a frequency day" in {

        val modelList =
          List(investmentOrder, investmentOrder.copy(frequency = investmentOrderFrequency.copy(day = None)))

        a[FrequencyDayNotDefined] should be thrownBy InvestmentOrderListViewMapper.from(modelList)
      }

    }
  }
}
