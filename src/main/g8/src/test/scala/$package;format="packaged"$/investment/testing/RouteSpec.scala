package $package$.investment.testing

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import spray.json.{DefaultJsonProtocol, JsObject}

trait RouteSpec extends AnyWordSpec with ScalatestRouteTest with SprayJsonSupport with DefaultJsonProtocol with Matchers {

  def errorMessageFromResponse(): String =
    responseAs[JsObject].fields("errorMessage").convertTo[String]
}
