package $package$.investment.testing

import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.testkit.TestKit
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.BeforeAndAfterAll

import scala.concurrent.ExecutionContext

trait ActorSystemSpec extends AnyWordSpec with BeforeAndAfterAll with ScalaFutures {

  implicit val system: ActorSystem = ActorSystem(suiteName)
  implicit val materializer: Materializer = Materializer(system)
  implicit val dispatcher: ExecutionContext = system.dispatcher

  override def afterAll(): Unit = {
    super.afterAll()
    TestKit.shutdownActorSystem(system)
  }
}
