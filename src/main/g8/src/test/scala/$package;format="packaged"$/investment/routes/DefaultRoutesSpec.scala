package $package$.investment.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Route.seal
import akka.http.scaladsl.testkit.RouteTestTimeout
import $package$.commons.rest.routes.DefaultRoutesFactory
import $package$.commons.rest.service.{Service, ServiceFactory}
import $package$.investment.BuildInfo
import $package$.investment.Routes
import $package$.investment.testing.{PatienceConfig, RouteSpec}
import akka.testkit.TestDuration
import scala.concurrent.duration._

class DefaultRoutesSpec extends RouteSpec with SprayJsonSupport with PatienceConfig {

  private val service: Service =
    ServiceFactory("localhost", 1234, BuildInfo)
  private val defaultRoutes: Route =
    new DefaultRoutesFactory(service, true).defaultRoutes
  private val routes = new Routes(defaultRoutes).routes

  implicit val timeout = RouteTestTimeout(5.seconds.dilated)

  "GET /version" should {
    "respond 200 OK" in {
      Get("/version") ~> seal(routes) ~> check {
        status shouldBe StatusCodes.OK
      }
    }
  }

  "GET /metrics" should {
    "respond 200 OK" in {
      Get("/metrics") ~> seal(routes) ~> check {
        status shouldBe StatusCodes.OK
      }
    }
  }

  "GET /docs/swagger.json" should {
    "respond 200 OK" in {
      Get("/docs/swagger.json") ~> seal(routes) ~> check {
        status shouldBe StatusCodes.OK
      }
    }
  }
}
