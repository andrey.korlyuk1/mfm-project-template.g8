package $package$.investment.routes.external.validation

import $package$.investment.dal.PortfolioDao
import $package$.investment.model.{GoalId, LedgerPortfolioId, Portfolio, PortfolioStatus}
import $package$.investment.routes.external.fixture.InvestmentOrderFixture
import $package$.investment.testing.ActorSystemSpec
import $package$.investment.validation.{FieldError, FundingRequestError, NumberWithinRangeValidationError}
import org.mockito.Mockito
import org.mockito.scalatest.IdiomaticMockito
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Future

class CreateInvestmentOrderRequestValidatorSpec
    extends ActorSystemSpec
    with IdiomaticMockito
    with BeforeAndAfterEach
    with Matchers
    with InvestmentOrderFixture {

  val portfolioDaoMock = mock[PortfolioDao]
  override def beforeEach(): Unit = {
    Mockito.reset(portfolioDaoMock)
    super.beforeEach()
  }

  val subject = new CreateInvestmentOrderRequestValidator(portfolioDaoMock)

  "CreateInvestmentOrderRequestValidator" when {

    "validate" should {
      val portfolio =
        Portfolio(goalId = GoalId(100), ledgerPortfolioId = LedgerPortfolioId(200), portfolioId = portfolioId, status = PortfolioStatus.Active)

      "successfully validate CreateInvestmentOrderRequest" in {

        portfolioDaoMock.getPortfolio(userId, portfolioId) returns Future.successful(portfolio)
        portfolioDaoMock.validateFundingRequest(userId,
                                                portfolio.goalId,
                                                createInvestmentOrderRequest.amount,
                                                createInvestmentOrderRequest.frequency.`type`) returns Future.successful(true)

        subject.validate((createInvestmentOrderRequest, userId.value)).futureValue shouldBe List.empty
      }

      s"fail with a ValidationError if the frequency day is less than \${CreateInvestmentOrderRequestValidator.minimumInvestmentOrderFrequencyDay}" in {

        portfolioDaoMock.getPortfolio(userId, portfolioId) returns Future.successful(portfolio)
        portfolioDaoMock.validateFundingRequest(userId,
                                                portfolio.goalId,
                                                createInvestmentOrderRequest.amount,
                                                createInvestmentOrderRequest.frequency.`type`) returns Future.successful(true)

        val expectedResult = List(
          NumberWithinRangeValidationError(
            0,
            CreateInvestmentOrderRequestValidator.minimumInvestmentOrderFrequencyDay,
            CreateInvestmentOrderRequestValidator.maximumInvestmentOrderFrequencyDay,
            "frequency.day"
          ))

        subject
          .validate((createInvestmentOrderRequest.copy(frequency = investmentOrderFrequencyView.copy(day = 0)), userId.value))
          .futureValue shouldBe expectedResult
      }

      s"fail with a ValidationError if the frequency day is bigger than \${CreateInvestmentOrderRequestValidator.maximumInvestmentOrderFrequencyDay}" in {

        portfolioDaoMock.getPortfolio(userId, portfolioId) returns Future.successful(portfolio)
        portfolioDaoMock.validateFundingRequest(userId,
                                                portfolio.goalId,
                                                createInvestmentOrderRequest.amount,
                                                createInvestmentOrderRequest.frequency.`type`) returns Future.successful(true)

        val expectedResult = List(
          NumberWithinRangeValidationError(
            29,
            CreateInvestmentOrderRequestValidator.minimumInvestmentOrderFrequencyDay,
            CreateInvestmentOrderRequestValidator.maximumInvestmentOrderFrequencyDay,
            "frequency.day"
          ))

        subject
          .validate((createInvestmentOrderRequest.copy(frequency = investmentOrderFrequencyView.copy(day = 29)), userId.value))
          .futureValue shouldBe expectedResult
      }

      s"fail with a ValidationError if the frequency day is bigger than \${CreateInvestmentOrderRequestValidator.maximumInvestmentOrderFrequencyDay} and portfolio do not belong to user" in {

        portfolioDaoMock.getPortfolio(userId, portfolioId) returns Future.successful(portfolio)
        portfolioDaoMock.validateFundingRequest(userId,
                                                portfolio.goalId,
                                                createInvestmentOrderRequest.amount,
                                                createInvestmentOrderRequest.frequency.`type`) returns Future.successful(true)

        val expectedResult = List(
          NumberWithinRangeValidationError(
            29,
            CreateInvestmentOrderRequestValidator.minimumInvestmentOrderFrequencyDay,
            CreateInvestmentOrderRequestValidator.maximumInvestmentOrderFrequencyDay,
            "frequency.day"
          )
        )

        subject
          .validate((createInvestmentOrderRequest.copy(frequency = investmentOrderFrequencyView.copy(day = 29)), userId.value))
          .futureValue should contain allElementsOf expectedResult
      }

      s"fail with a ValidationError if the frequency day is bigger than \${CreateInvestmentOrderRequestValidator.maximumInvestmentOrderFrequencyDay} and portfolio is not a valid UUID" in {

        val expectedResult = List(
          NumberWithinRangeValidationError(
            29,
            CreateInvestmentOrderRequestValidator.minimumInvestmentOrderFrequencyDay,
            CreateInvestmentOrderRequestValidator.maximumInvestmentOrderFrequencyDay,
            "frequency.day"
          ),
          FieldError("destination")
        )

        subject
          .validate(
            (createInvestmentOrderRequest.copy(destination = "destination", frequency = investmentOrderFrequencyView.copy(day = 29)), userId.value))
          .futureValue should contain allElementsOf expectedResult
      }

      s"fail with a Validation error if the portfolio is disabled" in {
        val disabledPortfolio = portfolio.copy(status = PortfolioStatus.Disabled)

        portfolioDaoMock.getPortfolio(userId, portfolioId) returns Future.successful(disabledPortfolio)

        val expectedResult = List(FieldError("destination"))

        subject.validate((createInvestmentOrderRequest, userId.value)).futureValue should contain allElementsOf expectedResult
      }

      s"fail with a Validation error if funding request is not validated" in {
        portfolioDaoMock.getPortfolio(userId, portfolioId) returns Future.successful(portfolio)
        portfolioDaoMock.validateFundingRequest(userId,
                                                portfolio.goalId,
                                                createInvestmentOrderRequest.amount,
                                                createInvestmentOrderRequest.frequency.`type`) returns Future.successful(false)

        val expectedResult = List(FundingRequestError(portfolioId, "destination"))

        subject.validate((createInvestmentOrderRequest, userId.value)).futureValue should contain allElementsOf expectedResult
      }

    }
  }
}
