package $package$.investment.application

import $package$.commons.kafka.Topic
import $package$.events.{PortfolioEvent, PortfolioEvents}
import $package$.investment.applications.PortfoliosStreams
import $package$.investment.event.consumer.InvestmentOrderEventHandler
import $package$.investment.model.Event
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient
import org.apache.kafka.streams.{TestInputTopic, TestOutputTopic, Topology, TopologyTestDriver}
import org.mockito.scalatest.IdiomaticMockito
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.util.UUID
import scala.concurrent.Future

class PortfoliosStreamsSpec extends AnyWordSpec with Matchers with IdiomaticMockito {

  val topicName = "portfolios"
  val schemaRegistry = new MockSchemaRegistryClient()
  val topic: Topic[String, PortfolioEvent] = Topic.mkStringKeyedTopic[PortfolioEvent, PortfolioEvents.PortfolioEvent](
    name = topicName,
    valueCompanion = PortfolioEvent,
    schemaRegistryClient = Some(schemaRegistry),
    serdeConfig = Map("schema.registry.url" -> "http://dummy:1234")
  )

  trait Fixture {
    val eventHandler: InvestmentOrderEventHandler = mock[InvestmentOrderEventHandler]
    val subject = new PortfoliosStreams(topic, eventHandler)

    val topology: Topology = subject.topology
    val testDriver = new TopologyTestDriver(topology)
    val inputTopic: TestInputTopic[String, PortfolioEvent] =
      testDriver.createInputTopic(topicName, topic.keySerde.serializer(), topic.valueSerde.serializer())
    val outputTopic: TestOutputTopic[String, PortfolioEvent] =
      testDriver.createOutputTopic(topicName, topic.keySerde.deserializer(), topic.valueSerde.deserializer())

  }

  "PortfoliosStreamsSpec" should {

    val portfolioEvent = PortfolioEvent(
      id = UUID.randomUUID().toString,
      eventType = PortfolioEvent.EventType.PortfolioCapabilitiesBlocked(
        PortfolioEvent.PortfolioCapabilitiesBlocked()
      )
    )

    "be able to consume PortfolioEvent event" in new Fixture {
      eventHandler.handleEvent(portfolioEvent) returns Future.successful(Right(Event.Succeed(portfolioEvent)))

      inputTopic.pipeInput("1", portfolioEvent)
      outputTopic.isEmpty shouldBe true

      testDriver.close()
    }

    "handle an Event.Ignored" in new Fixture {

      eventHandler.handleEvent(portfolioEvent) returns Future.successful(Right(Event.Ignored(portfolioEvent)))

      inputTopic.pipeInput("2", portfolioEvent)
      outputTopic.isEmpty shouldBe true

      testDriver.close()
    }

    "handle an Event.Failed" in new Fixture {
      val error: Event.Failed[PortfolioEvent] = Event.Failed(portfolioEvent, new RuntimeException("TEST"))

      eventHandler.handleEvent(portfolioEvent) returns Future.successful(Left(error))

      inputTopic.pipeInput("3", portfolioEvent)
      outputTopic.isEmpty shouldBe true

      testDriver.close()
    }

  }

}
