package $package$.investment.testing

import java.time.LocalDateTime

import org.mockito.ArgumentMatcher
import org.scalatest.matchers.{BeMatcher, MatchResult}

trait CustomMatchers {

  class EntitiesMatcher[T <: Product](`this`: T) extends BeMatcher[T] with ArgumentMatcher[T] {
    override def apply(that: T): MatchResult =
      MatchResult(
        productsMatch(`this`, that),
        s"\${`this`} is not the same as \$that",
        s"\${`this`} is the same as \$that"
      )

    def matches(that: T): Boolean = this(that).matches
  }

  class EntitiesMatcherSeq[T <: Product](`this`: Seq[T]) extends BeMatcher[Seq[T]] with ArgumentMatcher[Seq[T]] {
    override def apply(that: Seq[T]): MatchResult = {
      val isMatch = that.size == `this`.size && that.forall(thatElem => `this`.exists(productsMatch(_, thatElem)))
      MatchResult(
        isMatch,
        s"\${`this`} is not the same as \$that",
        s"\${`this`} is the same as \$that"
      )
    }

    def matches(that: Seq[T]): Boolean = this(that).matches
  }

  private def productsMatch(product1: Product, product2: Product): Boolean = {
    val fieldPairs = product1.productIterator zip product2.productIterator
    fieldPairs.forall {
      case (left: Seq[_], right: Seq[_]) =>
        left.size == right.size && left.forall(el => right.contains(el))
      case (right, left) => right == left
    }
  }

  def theSameAs[T <: Product](`this`: T) = new EntitiesMatcher(`this`)
  def theSameAsSeq[T <: Product](`this`: Seq[T]) =
    new EntitiesMatcherSeq(`this`)

  implicit val localDateTimeOrdering: Ordering[LocalDateTime] =
    (left: LocalDateTime, right: LocalDateTime) => left.compareTo(right)
}
