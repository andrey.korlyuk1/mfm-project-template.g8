package $package$.investment.routes.internal

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID
import akka.http.scaladsl.server.Route.seal
import akka.http.scaladsl.server.directives.RouteDirectives
import $package$.commons.akka.http.job.JobRoutesTestSupport
import $package$.investment.model.InvestmentOrderId
import $package$.investment.routes.internal.json.JobsProtocol
import $package$.investment.routes.internal.model.TriggerDailyJobRequest
import $package$.investment.service.InvestmentExecutionService
import $package$.investment.service.InvestmentExecutionService.{ExecutionFailure, ExecutionReport}
import $package$.investment.testing.{FixedClock, RouteSpec}
import $package$.investment.{Routes => GlobalRoutes}
import $package$.util.jobs.{InMemoryJobMonitor, JobInstance}
import org.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.Future
import scala.concurrent.duration._

class JobsRoutesSpec
    extends RouteSpec
    with BeforeAndAfterEach
    with JobsProtocol
    with MockitoSugar
    with FixedClock
    with JobRoutesTestSupport
    with ScalaFutures {

  override protected val fixedDateTime = LocalDateTime.now().withNano(0)

  private val investmentExecutionServiceMock = mock[InvestmentExecutionService]

  class RoutesUnderTest() extends GlobalRoutes(RouteDirectives.reject) {
    override val investmentExecutionService = investmentExecutionServiceMock
    override val jobMonitor = new InMemoryJobMonitor(1.minute)(fixedClock)
  }

  val subject = new RoutesUnderTest

  override def beforeEach(): Unit =
    reset(investmentExecutionServiceMock)

  "POST /jobs/investment-orders/trigger" should {

    val executionDate = LocalDate.of(2020, 1, 1)
    val triggerDailyJobRequest =
      TriggerDailyJobRequest(date = Some(executionDate))
    val successfulReport = ExecutionReport(List.empty, List.empty)

    "return 202 Accepted and have a successful job" in {
      when(investmentExecutionServiceMock.executeInvestmentOrders(executionDate)).thenReturn(Future.successful(successfulReport))

      Post(s"/jobs/investment-orders/trigger", triggerDailyJobRequest) ~> seal(subject.routes) ~> check {
        checkJobEndedWithStatus(subject.routes)(InvestmentExecutionService.InvestmentOrdersJobName, JobInstance.Status.Completed).futureValue
      }
    }

    "succeed using today as default date if this one is not provided in the request" in {
      val request = triggerDailyJobRequest.copy(date = None)
      when(investmentExecutionServiceMock.executeInvestmentOrders(LocalDate.now(fixedClock))).thenReturn(Future.successful(successfulReport))

      Post(s"/jobs/investment-orders/trigger", request) ~> seal(subject.routes) ~> check {
        checkJobEndedWithStatus(subject.routes)(InvestmentExecutionService.InvestmentOrdersJobName, JobInstance.Status.Completed).futureValue
      }
    }

    "return 202 Accepted and make the job fail if the service fails" in {
      when(investmentExecutionServiceMock.executeInvestmentOrders(executionDate)).thenReturn(Future.failed(new RuntimeException))

      Post(s"/jobs/investment-orders/trigger", triggerDailyJobRequest) ~> seal(subject.routes) ~> check {
        checkJobEndedWithStatus(subject.routes)(InvestmentExecutionService.InvestmentOrdersJobName, JobInstance.Status.Failed).futureValue
      }
    }

    "return 202 Accepted and make the job fail if the service returns an unsuccessful report" in {

      val unsuccessfulReport =
        ExecutionReport(List(ExecutionFailure(InvestmentOrderId(UUID.randomUUID), new RuntimeException)), List.empty)

      when(investmentExecutionServiceMock.executeInvestmentOrders(executionDate)).thenReturn(Future.successful(unsuccessfulReport))

      Post(s"/jobs/investment-orders/trigger", triggerDailyJobRequest) ~> seal(subject.routes) ~> check {
        checkJobEndedWithStatus(subject.routes)(InvestmentExecutionService.InvestmentOrdersJobName, JobInstance.Status.Failed).futureValue
      }
    }

  }
}
