package $package$.investment.service

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import akka.stream.scaladsl.Source
import $package$.investment.dal._
import $package$.investment.mapper.PortfolioMapper.PortfolioMappingError
import $package$.investment.model._
import $package$.investment.service.InvestmentExecutionService.{DisabledOrder, ExecutionFailure, ExecutionReport}
import $package$.investment.service.InvestmentExecutor.{InactivePortfolioError, PortfolioWithNoLedgerError}
import $package$.investment.testing.{ActorSystemSpec, FixedClock, PatienceConfig}
import org.mockito.scalatest.MockitoSugar
import org.scalatest.OptionValues
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration._
import scala.concurrent.Future

class InvestmentExecutionServiceSpec extends ActorSystemSpec with MockitoSugar with Matchers with FixedClock with PatienceConfig with OptionValues {

  override protected val fixedDateTime = LocalDateTime.now()

  val processingDate = LocalDate.now

  "InvestmentExecutor" when {

    val someFailure =
      ExecutionFailure(InvestmentOrderId(UUID.randomUUID), new RuntimeException)
    val someDisabledOrder =
      DisabledOrder(InvestmentOrderId(UUID.randomUUID), new RuntimeException)

    val userId = UserId(1)
    val portfolioId = PortfolioId(UUID.randomUUID)

    val orderId = InvestmentOrderId(UUID.randomUUID)

    val investmentOrder = InvestmentOrder(
      id = orderId,
      userId = userId,
      amount = 100,
      destination = portfolioId,
      frequency = InvestmentOrderFrequency(FrequencyType.Monthly, Some(3)),
      active = true
    )

    val ledgerPortfolioId = LedgerPortfolioId(10)
    val goalId = GoalId(1000)
    val portfolio = Portfolio(goalId, portfolioId, ledgerPortfolioId, PortfolioStatus.Active)

    val cashFlow = CashFlow(
      ledgerPortfolioId = ledgerPortfolioId,
      amount = investmentOrder.amount,
      reason = "Available cash recurring investment"
    )

    val cashFlowId = CashFlowId(100)

    class Fixture {

      val cashFlowDaoMock: CashFlowDao = mock[CashFlowDao]
      val investmentOrderDaoMock: InvestmentOrderDao = mock[InvestmentOrderDao]
      val portfolioDaoMock: PortfolioDao = mock[PortfolioDao]
      val investmentDaoMock: InvestmentDao = mock[InvestmentDao]
      val cashServiceMock: CashService = mock[CashService]

      val subject =
        new InvestmentExecutor(cashFlowDaoMock, investmentOrderDaoMock, portfolioDaoMock, investmentDaoMock, cashServiceMock)(List(someFailure),
                                                                                                                              List(someDisabledOrder))

    }

    "execute" should {

      "execute the order and save the new investment" when {
        "the portfolio available cash is higher than the order amount" in new Fixture {

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio))
          when(cashFlowDaoMock.createCashFlow(cashFlow)).thenReturn(Future.successful(cashFlowId))
          when(investmentDaoMock.saveInvestment(investmentOrder, cashFlowId, processingDate)).thenReturn(Future.unit)

          subject.execute(investmentOrder, processingDate).futureValue shouldBe subject

        }
      }

      "execute the order, save the new investment and disable the order" when {
        "the portfolio available chas is higher than 0 but lower than the order amount" in new Fixture {

          val availableCash = AvailableCash(50)
          val error = NotAvailableCashException(ledgerPortfolioId)

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio))
          when(cashFlowDaoMock.createCashFlow(cashFlow)).thenReturn(Future.failed(error))
          when(cashServiceMock.getAvailableCash(ledgerPortfolioId)).thenReturn(Future.successful(availableCash))
          when(cashFlowDaoMock.createCashFlow(cashFlow.copy(amount = availableCash.value))).thenReturn(Future.successful(cashFlowId))
          when(investmentDaoMock.saveInvestment(investmentOrder.copy(amount = availableCash.value), cashFlowId, processingDate))
            .thenReturn(Future.unit)
          when(investmentOrderDaoMock.deactivateInvestmentOrder(userId, orderId)).thenReturn(Future.successful(1))

          val result = subject.execute(investmentOrder, processingDate).futureValue
          result.disabledOrders shouldBe DisabledOrder(orderId, error) :: subject.disabledOrders
          result.failures shouldBe subject.failures
        }
      }

      "disable the order" when {
        "the portfolio available chas is zero" in new Fixture {

          val availableCash = AvailableCash(0)
          val error = NotAvailableCashException(ledgerPortfolioId)

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio))
          when(cashFlowDaoMock.createCashFlow(cashFlow)).thenReturn(Future.failed(error))
          when(cashServiceMock.getAvailableCash(ledgerPortfolioId)).thenReturn(Future.successful(availableCash))
          when(investmentOrderDaoMock.deactivateInvestmentOrder(userId, orderId)).thenReturn(Future.successful(1))

          val result = subject.execute(investmentOrder, processingDate).futureValue
          result.disabledOrders shouldBe DisabledOrder(orderId, error) :: subject.disabledOrders
          result.failures shouldBe subject.failures
        }
      }

      "disable the order if the portfolio is disabled" in new Fixture {

        when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio.copy(status = PortfolioStatus.Disabled)))
        when(investmentOrderDaoMock.deactivateInvestmentOrder(userId, orderId)).thenReturn(Future.successful(1))

        val result = subject.execute(investmentOrder, processingDate).futureValue
        result.disabledOrders shouldBe DisabledOrder(orderId, InactivePortfolioError(orderId)) :: subject.disabledOrders
        result.failures shouldBe subject.failures

      }

      "skip the order and add PortfolioWithNoLedgerError if the portfolio has no ledger id" in new Fixture {
        val error = mock[PortfolioMappingError]
        when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.failed(error))

        val result = subject.execute(investmentOrder, processingDate).futureValue
        result.disabledOrders shouldBe subject.disabledOrders
        result.failures shouldBe ExecutionFailure(orderId, PortfolioWithNoLedgerError(orderId)) :: subject.failures

      }

      "skip the order and accumulate the error" when {

        val error = new RuntimeException("test")

        "getting the portfolio fails" in new Fixture {

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.failed(error))

          val result = subject.execute(investmentOrder, processingDate).futureValue
          result.disabledOrders shouldBe subject.disabledOrders
          result.failures shouldBe ExecutionFailure(orderId, error) :: subject.failures

        }

        "creating the cash-flow fails" in new Fixture {

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio))
          when(cashFlowDaoMock.createCashFlow(cashFlow)).thenReturn(Future.failed(error))

          val result = subject.execute(investmentOrder, processingDate).futureValue
          result.disabledOrders shouldBe subject.disabledOrders
          result.failures shouldBe ExecutionFailure(orderId, error) :: subject.failures

        }

        "saving the new investment fails" in new Fixture {

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio))
          when(cashFlowDaoMock.createCashFlow(cashFlow)).thenReturn(Future.successful(cashFlowId))
          when(investmentDaoMock.saveInvestment(investmentOrder, cashFlowId, processingDate)).thenReturn(Future.failed(error))

          val result = subject.execute(investmentOrder, processingDate).futureValue
          result.disabledOrders shouldBe subject.disabledOrders
          result.failures shouldBe ExecutionFailure(orderId, error) :: subject.failures

        }

        "disabling the order fails" in new Fixture {

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio.copy(status = PortfolioStatus.Disabled)))
          when(investmentOrderDaoMock.deactivateInvestmentOrder(userId, orderId)).thenReturn(Future.failed(error))

          val result = subject.execute(investmentOrder, processingDate).futureValue
          result.disabledOrders shouldBe subject.disabledOrders
          result.failures shouldBe ExecutionFailure(orderId, error) :: subject.failures

        }

        "getting the available cash fails" in new Fixture {

          val error = NotAvailableCashException(ledgerPortfolioId)
          val exception = new RuntimeException("TEST")

          when(portfolioDaoMock.getPortfolio(userId, portfolioId)).thenReturn(Future.successful(portfolio))
          when(cashFlowDaoMock.createCashFlow(cashFlow)).thenReturn(Future.failed(error))
          when(cashServiceMock.getAvailableCash(ledgerPortfolioId)).thenReturn(Future.failed(exception))

          val result = subject.execute(investmentOrder, processingDate).futureValue
          result.disabledOrders shouldBe subject.disabledOrders
          result.failures shouldBe ExecutionFailure(orderId, exception) :: subject.failures

        }

      }

    }

    "report" should {

      "return an ExecutionReport containing all the errors and disabled orders" in new Fixture {

        subject.report shouldBe ExecutionReport(subject.failures, subject.disabledOrders)

      }

    }

  }

  "ExecutionReport" when {

    "toString" should {

      "return a properly formatted textual report" in {

        val failure1 = ExecutionFailure(InvestmentOrderId(UUID.randomUUID), new RuntimeException)
        val failure2 = ExecutionFailure(InvestmentOrderId(UUID.randomUUID), new RuntimeException)
        val disabledOrder1 = DisabledOrder(InvestmentOrderId(UUID.randomUUID), new RuntimeException)
        val disabledOrder2 = DisabledOrder(InvestmentOrderId(UUID.randomUUID), new RuntimeException)

        val subject = ExecutionReport(List(failure1, failure2), List(disabledOrder1, disabledOrder2))

        subject.toString shouldBe
        s"""
              |Failed to execute 2 investment orders:
              |Investment order: \${failure1.investmentOrderId.value} execution failed with error: \${failure1.error}
              |Investment order: \${failure2.investmentOrderId.value} execution failed with error: \${failure2.error}
              |Disabled 2 investment orders:
              |Disabled order: \${disabledOrder1.investmentOrderId.value} due to: \${disabledOrder1.reason}
              |Disabled order: \${disabledOrder2.investmentOrderId.value} due to: \${disabledOrder2.reason}
              |""".stripMargin

      }

    }

  }

  "InvestmentExecutionService" when {

    class Fixture {

      val cashFlowDaoMock: CashFlowDao = mock[CashFlowDao]
      val investmentOrderDaoMock: InvestmentOrderDao = mock[InvestmentOrderDao]
      val portfolioDaoMock: PortfolioDao = mock[PortfolioDao]
      val investmentDaoMock: InvestmentDao = mock[InvestmentDao]
      val cashServiceMock: CashService = mock[CashService]
      val emptyExecutorMock = mock[InvestmentExecutor]

      val subject =
        new InvestmentExecutionService(investmentOrderDaoMock, cashFlowDaoMock, portfolioDaoMock, investmentDaoMock, cashServiceMock)(
          throttlingQuantity = 10,
          throttlingInterval = 1.second) {
          override val emptyExecutor = emptyExecutorMock
        }

    }

    val date = LocalDate.now

    "executeInvestmentOrders" should {

      "do nothing if there are no orders for the given date" in new Fixture {

        val emptyReport = ExecutionReport(List.empty, List.empty)

        when(emptyExecutorMock.report).thenReturn(emptyReport)

        when(investmentOrderDaoMock.getUnprocessedInvestmentOrderStream(date)).thenReturn(Source.empty)

        subject.executeInvestmentOrders(date).futureValue shouldBe emptyReport

      }

      "call the executor for each order and return the last report" in new Fixture {

        val order1 = mock[InvestmentOrder]
        val order2 = mock[InvestmentOrder]

        val executor1 = mock[InvestmentExecutor]
        val executor2 = mock[InvestmentExecutor]

        val report = mock[ExecutionReport]

        when(investmentOrderDaoMock.getUnprocessedInvestmentOrderStream(date)).thenReturn(Source(List(order1, order2)))
        when(emptyExecutorMock.execute(order1, processingDate)).thenReturn(Future.successful(executor1))
        when(executor1.execute(order2, processingDate)).thenReturn(Future.successful(executor2))
        when(executor2.report).thenReturn(report)

        subject.executeInvestmentOrders(date).futureValue shouldBe report

      }

      "fail" when {

        val error = new RuntimeException("error")

        "getting the investment orders fails" in new Fixture {

          when(investmentOrderDaoMock.getUnprocessedInvestmentOrderStream(date)).thenReturn(Source.failed(error))

          subject.executeInvestmentOrders(date).failed.futureValue shouldBe error

        }

        "any executor fails" in new Fixture {

          val order1 = mock[InvestmentOrder]

          when(investmentOrderDaoMock.getUnprocessedInvestmentOrderStream(date)).thenReturn(Source(List(order1)))
          when(emptyExecutorMock.execute(order1, processingDate)).thenReturn(Future.failed(error))

          subject.executeInvestmentOrders(date).failed.futureValue shouldBe error

        }

      }

    }

  }

}
