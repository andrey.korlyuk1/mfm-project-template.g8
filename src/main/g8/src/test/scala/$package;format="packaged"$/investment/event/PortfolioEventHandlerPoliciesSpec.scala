package $package$.investment.event

import $package$.events.PortfolioEvent
import $package$.events.PortfolioEvent.{PortfolioCapability, PortfolioFunding}
import $package$.investment.event.consumer.InvestmentOrderEventHandlerPolicies
import $package$.investment.model.{DeactivateInvestmentOrderDomainEvent, IgnoreCommand, PortfolioId}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.util.UUID

class PortfolioEventHandlerPoliciesSpec extends AnyWordSpec with Matchers {

  "PortfolioEventHandlerPolicies" when {
    "from" should {
      val portfolioId = PortfolioId(UUID.randomUUID())
      val portfolioCapability =
        PortfolioCapability(capability = $package$.events.PortfolioEvent.PortfolioCapability.Capability.PortfolioFunding(PortfolioFunding()))
      val portfolioEvent = PortfolioEvent(
        id = portfolioId.value.toString,
        eventType = PortfolioEvent.EventType.PortfolioCapabilitiesBlocked(
          PortfolioEvent.PortfolioCapabilitiesBlocked(blockedCapabilities = scala.Seq(portfolioCapability))
        )
      )

      "map portfolioEvent to DeactivateInvestmentOrderCommand" in {
        val expectedResult = DeactivateInvestmentOrderCommand(portfolioId)

        InvestmentOrderEventHandlerPolicies.from(portfolioEvent) shouldBe expectedResult
      }

      "map portfolioEvent to IgnoreCommand" in {
        val portfolioEventWithoutBlockedCapabilities = PortfolioEvent(
          id = portfolioId.value.toString,
          eventType = PortfolioEvent.EventType.PortfolioCapabilitiesBlocked(
            PortfolioEvent.PortfolioCapabilitiesBlocked()
          )
        )
        val expectedResult = IgnoreCommand

        InvestmentOrderEventHandlerPolicies.from(portfolioEventWithoutBlockedCapabilities) shouldBe expectedResult
      }
    }
  }

}
