package $package$.investment.dal

import java.util.UUID

import $package$.investment.dal.infra.rest.portfolio.PortfolioApi
import $package$.investment.dal.infra.rest.portfolio.mapper.FundingRequestValidationContractMapper
import $package$.investment.dal.infra.rest.portfolio.model.{
  FundingRequestValidationContract,
  FundingRequestValidationResponseContract,
  PortfolioResponseContract
}
import $package$.investment.mapper.PortfolioMapper
import $package$.investment.mapper.PortfolioMapper.PortfolioMappingError
import $package$.investment.model.{FrequencyType, GoalId, PortfolioId, UserId}
import $package$.investment.testing.ActorSystemSpec
import org.mockito.scalatest.IdiomaticMockito
import org.scalatest.TryValues
import org.scalatest.concurrent.IntegrationPatience
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Future

class PortfolioDaoSpec extends ActorSystemSpec with IdiomaticMockito with Matchers with IntegrationPatience with TryValues {

  class Fixture {

    val portfolioApiMock = mock[PortfolioApi]

    val portfolioDao = new PortfolioDao(portfolioApiMock)

  }

  val userId = 1L
  val portfolioId = UUID.fromString("75d77e0a-1790-4144-97ac-49b9ff261f96")
  val goalId = 563455L

  val apiResponse = PortfolioResponseContract(
    goalId = Some(goalId),
    externalId = UUID.fromString("75d77e0a-1790-4144-97ac-49b9ff261f96"),
    ledgerPortfolioId = Some(652178L),
    name = "Portfolio 1",
    status = "Active"
  )

  "PortfolioDao" when {

    "getPortfolio" should {
      val expectedResponse = PortfolioMapper.from(apiResponse)

      "return the portfolio" in new Fixture {

        portfolioApiMock.getUserPortfolio(userId, portfolioId) returns Future.successful(apiResponse)

        portfolioDao.getPortfolio(UserId(userId), PortfolioId(portfolioId)).futureValue shouldBe expectedResponse.success.value

      }

      "fail if the portfolio contract cannot be mapped to a valid Portfolio" in new Fixture {

        val apiResponseWithNoGoalId: PortfolioResponseContract = apiResponse.copy(goalId = None)
        val expectedError = PortfolioMappingError(apiResponseWithNoGoalId)

        portfolioApiMock.getUserPortfolio(userId, portfolioId) returns Future.successful(apiResponseWithNoGoalId)

        portfolioDao.getPortfolio(UserId(userId), PortfolioId(portfolioId)).failed.futureValue shouldBe expectedError
      }

      "fail if the api call fails" in new Fixture {

        val error = new RuntimeException("test")

        portfolioApiMock.getUserPortfolio(userId, portfolioId) returns Future.failed(error)

        portfolioDao.getPortfolio(UserId(userId), PortfolioId(portfolioId)).failed.futureValue shouldBe error

      }

    }

    "validateFundingRequest" should {
      val amount = 100
      val frequencyType = FrequencyType.Monthly
      val fundingRequestValidationContract = mock[FundingRequestValidationContract]

      Seq(true, false).foreach { isValid =>
        s"return \$isValid when the api returns \$isValid" in new Fixture {
          val apiResponse = FundingRequestValidationResponseContract(isValid)

          withObjectMocked[FundingRequestValidationContractMapper.type] {

            FundingRequestValidationContractMapper.from(amount, frequencyType) returns fundingRequestValidationContract
            portfolioApiMock.validateFundingRequest(userId, goalId, fundingRequestValidationContract) returns Future.successful(apiResponse)

            portfolioDao.validateFundingRequest(UserId(userId), GoalId(goalId), amount, frequencyType).futureValue shouldBe isValid
          }

        }
      }

      "fails when the api call fails" in new Fixture {
        val expectedError = new RuntimeException("error")
        withObjectMocked[FundingRequestValidationContractMapper.type] {

          FundingRequestValidationContractMapper.from(amount, frequencyType) returns fundingRequestValidationContract
          portfolioApiMock.validateFundingRequest(userId, goalId, fundingRequestValidationContract) returns Future.failed(expectedError)

          portfolioDao.validateFundingRequest(UserId(userId), GoalId(goalId), amount, frequencyType).failed.futureValue shouldBe expectedError
        }
      }
    }
  }

}
