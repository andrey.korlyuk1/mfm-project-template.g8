package $package$.investment.dal

import akka.stream.scaladsl.{Sink, Source}
import $package$.investment.dal.infra.db.InvestmentOrderDB
import $package$.investment.fixture.InvestmentOrderFixture
import $package$.investment.model.{InvestmentOrderId, PortfolioId}
import $package$.investment.testing.{ActorSystemSpec, CustomMatchers}
import $package$.util.UUIDGenerator
import org.mockito.scalatest.IdiomaticMockito
import org.scalatest.matchers.should.Matchers

import java.time.LocalDate
import java.util.UUID

class InvestmentOrderDaoSpec extends ActorSystemSpec with Matchers with IdiomaticMockito with InvestmentOrderFixture with CustomMatchers {

  class Fixture {
    val uuidGeneratorMock = mock[UUIDGenerator]
    val investmentOrderDBMock = mock[InvestmentOrderDB]

    val investmentOrderDao =
      new InvestmentOrderDao(investmentOrderDBMock, uuidGeneratorMock)
  }

  "InvestmentOrderDao" when {

    "createInvestmentOrder" should {

      val investmentOrderId = "4cc75b66-95e8-4657-922b-5cf274fd5d1c"
      val newInvestmentOrderEntity =
        investmentOrderEntity.copy(id = investmentOrderId)
      val expectedInvestmentOrder =
        investmentOrder.copy(InvestmentOrderId(UUID.fromString(investmentOrderId)))

      "successfully create a new InvestmentOrder" in new Fixture {

        uuidGeneratorMock.randomUUID() returns UUID.fromString(investmentOrderId)
        investmentOrderDBMock.insert(argThat(theSameAs(newInvestmentOrderEntity))) returns newInvestmentOrderEntity

        investmentOrderDao.createInvestmentOrder(createInvestmentOrder).futureValue shouldBe expectedInvestmentOrder
      }

      "fail if investmentOrderDB fails creating InvestmentOrder" in new Fixture {
        val expectedException = new RuntimeException
        uuidGeneratorMock.randomUUID() returns UUID.fromString(investmentOrderId)
        investmentOrderDBMock.insert(argThat(theSameAs(newInvestmentOrderEntity))) throws expectedException

        investmentOrderDao.createInvestmentOrder(createInvestmentOrder).failed.futureValue shouldBe expectedException

      }

    }

    "getInvestmentOrders" should {

      "return the list of the investment orders that matches the filter" in new Fixture {

        val investmentOrderEntityList = List(investmentOrderEntity)
        val expectedResult = List(investmentOrder)
        investmentOrderDBMock.getList(investmentOrderFilter) returns investmentOrderEntityList

        investmentOrderDao.getInvestmentOrders(investmentOrderFilter).futureValue shouldBe expectedResult
      }

      "return an empty list if no investment orders are found" in new Fixture {

        investmentOrderDBMock.getList(investmentOrderFilter) returns List.empty

        investmentOrderDao.getInvestmentOrders(investmentOrderFilter).futureValue shouldBe List.empty
      }

      "fail if investmentOrderDB fails" in new Fixture {
        val expectedException = new RuntimeException

        investmentOrderDBMock.getList(investmentOrderFilter) throws expectedException

        investmentOrderDao.getInvestmentOrders(investmentOrderFilter).failed.futureValue shouldBe expectedException
      }

    }

    "deactivateInvestmentOrder" should {

      "return 1 if find and deactivate the investment order given by userId and investmentOrderId couple" in new Fixture {
        investmentOrderDBMock.deactivate(userId, investmentOrderId) returns 1

        investmentOrderDao.deactivateInvestmentOrder(userId, investmentOrderId).futureValue shouldBe 1
      }

      "return 0 if can't find any investment order given by userId and investmentOrderId couple" in new Fixture {
        investmentOrderDBMock.deactivate(userId, investmentOrderId) returns 0

        investmentOrderDao.deactivateInvestmentOrder(userId, investmentOrderId).futureValue shouldBe 0
      }

      "fail if the investmentOrderDao fails" in new Fixture {
        val expectedException = new RuntimeException
        investmentOrderDBMock.deactivate(userId, investmentOrderId) throws expectedException

        investmentOrderDao.deactivateInvestmentOrder(userId, investmentOrderId).failed.futureValue shouldBe expectedException
      }

    }

    "deactivateInvestmentOrdersByDestination" should {
      val destination = PortfolioId(UUID.randomUUID())

      "deactivate investment orders with the given destination" in new Fixture {
        val expectedResult = 2
        investmentOrderDBMock.deactivateByDestination(destination.value) returns expectedResult

        investmentOrderDao.deactivateInvestmentOrdersByDestination(destination).futureValue shouldBe expectedResult

      }

      "fails" when {
        "investmentOrderDB fails deactivating the investment orders" in new Fixture {
          val expectedError = new RuntimeException("TEST")
          investmentOrderDBMock.deactivateByDestination(destination.value) throws expectedError

          investmentOrderDao.deactivateInvestmentOrdersByDestination(destination).failed.futureValue shouldBe expectedError
        }
      }
    }

    "getUnprocessedInvestmentOrderStream" should {

      val date = LocalDate.now

      "return a stream of unprocessed investment orders for the specified date" in new Fixture {
        investmentOrderDBMock.getUnprocessedInvestmentOrderStream(date) returns Source(List(investmentOrderEntity))

        investmentOrderDao.getUnprocessedInvestmentOrderStream(date).runWith(Sink.seq).futureValue should contain theSameElementsAs List(
          investmentOrder)

      }

      "fail if loading the orders fails" in new Fixture {
        val error = new RuntimeException("test")

        investmentOrderDBMock.getUnprocessedInvestmentOrderStream(date) returns Source.failed(error)

        investmentOrderDao.getUnprocessedInvestmentOrderStream(date).runWith(Sink.ignore).failed.futureValue shouldBe error
      }

    }

  }
}
