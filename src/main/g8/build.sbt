import scala.io.Source

enablePlugins(JavaAppPackaging, BuildInfoPlugin, JavaAgent)

organization := "$package$"

name := "$name;format="normalize"$"

scalaVersion := "2.13.6"

resolvers += "confluent" at "https://packages.confluent.io/maven/"

val moneyfarmCommonsVersion = "$moneyfarmCommonsVersion$"
val akkaHttpVersion = "10.2.4"
val akkaVersion = "2.6.13"
val tapirVersion = "0.17.19"
$if(useKafka.truthy)$
val kafkaStreamsVersion = "3.0.0"
val kafkaProtobufVersion = "7.0.1"
$endif$
val scalaTestVersion = "3.2.10"
val mockitoScalaVersion = "1.16.46"

libraryDependencies ++= Seq(
  "$package$.commons" %% "akka-http-commons" % moneyfarmCommonsVersion,
  "$package$.commons" %% "akka-rest-commons" % moneyfarmCommonsVersion,
  "$package$.commons" %% "model-commons" % moneyfarmCommonsVersion,
  "$package$.commons" %% "spray-json-commons" % moneyfarmCommonsVersion,
  "$package$.commons" %% "http-problem-detail-commons" % moneyfarmCommonsVersion,
  //tapir
  "com.softwaremill.sttp.tapir" %% "tapir-akka-http-server" % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-json-spray" % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml" % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-akka-http" % tapirVersion,
  // tracing
  "$package$.commons" %% "http-tracing-commons" % moneyfarmCommonsVersion,
  $if(useKafka.truthy)$
  //kafka
  "$package$.commons" %% "kafka-commons" % moneyfarmCommonsVersion % "compile->compile;test->test",
  $endif$
  $if(setupDB.truthy)$
  // db
  "$package$.commons" %% "db-commons" % moneyfarmCommonsVersion,
  "mysql" % "mysql-connector-java" % "8.0.28",
  "com.zaxxer" % "HikariCP" % "4.0.3",
  $endif$
  $if(externalRoutesSupport.truthy)$
  // jwt
  "$package$.commons" %% "akka-http-jwt-commons" % moneyfarmCommonsVersion,
  $endif$
  // test
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test,it",
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test,it",
  "$package$.commons" %% "integration-test-commons" % moneyfarmCommonsVersion % "it",
  "org.mockito" %% "mockito-scala" % mockitoScalaVersion % "test",
  "org.mockito" %% "mockito-scala-scalatest" % mockitoScalaVersion % "test,it",
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test,it",
  $if(useBackgroundJobs.truthy)$
  // jobs
  "$package$.commons" %% "akka-http-job-commons" % moneyfarmCommonsVersion,
  "$package$.commons" %% "job-commons" % moneyfarmCommonsVersion,
  "$package$.commons" %% "job-db-commons" % moneyfarmCommonsVersion
  $endif$
)

ThisBuild / version := Source.fromFile("version").mkString.trim
ThisBuild / fork := true

$if(useKafka.truthy)$
Compile / PB.targets := Seq(
  PB.gens.java -> (Compile / sourceManaged).value / "javapb",
  scalapb.gen(flatPackage = true, javaConversions = true) -> (Compile / sourceManaged).value / "scalapb"
)
$endif$

lazy val $name;format="normalize"$ = project.in(file(".")).configs(IntegrationTest).settings(Defaults.itSettings)
