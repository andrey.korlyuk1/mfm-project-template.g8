# Changelog

## v0.12.3
**Chore**
* [FP1-3236] add filter to get investment orders

## v0.12.2
**Chore**
* [GROW-4314] Remove check permission flag

## v0.12.1
**Bug fix**
* [bugfix/SRE-2718_bump_version_and_fix_coverage] [skip ci]

**Chores**
* [GROW-4211] force check permissions
* [chore/bump_mcl] [skip ci]

## v0.12.0
**New feature**
* [feature/SRE-2618_add_notifier_manual_step] [skip ci]

**Bug fix**
* [hotfix/SRE-2649_deploy_step_dependency] [skip ci]

**Chore**
* Add external endpoint tto get investment orders

## v0.11.0
**New feature**
* [feature/SRE-2529-upgrade-project]

## v0.10.20
**Chore**
* [GROW-3289] update public endpoint to support permission

## v0.10.19
**Chore**
* Upgrade to scala-commons 15.x

## v0.10.18
**Chore**
* [INF-2304] Implement portfolio event handler

## v0.10.17
**Chore**
* [INF-2299] Disable investment order for portfolios with "add-funds" blocked capability

## v0.10.16
**Chore**
* [INF-2117] Integrate with kafka portfolios topic

## v0.10.15
**Chore**
* [BE-NIJ] Add test coverage and update some libraries

## v0.10.14
**Chore**
* SRE-2140 investment cleanup

## v0.10.13
**Chore**
* [SRE-2035] - Migrate to k8s

## v0.10.12
**Chore**
* NIJ - Upgraded to commons 10.2.1

## v0.10.11
**Bug fix**
* Upgrading pipelines

## v0.10.10
**Bug fix**
* [INF-NIJ] Fix investment job route

## v0.10.9
**Chore**
* [INF-NIJ] Update docker image

## v0.10.8
**Chore**
* [INF-NIJ] Update pipeline

## v0.10.7
**Chore**
* [BE-NIJ] migrate jobs routes in tapir

## v0.10.6
**Chore**
* Drop null values in generated json documentation

## v0.10.5
**Chore**
* [BE-165] tapir migration

## v0.10.4
**Chore**
* [BE-NIJ] Update scala-commons, sbt, sbt-mfm, scala, and set proper JAVA_OPTS

## v0.10.3
**Bug fix**
* [INF-83] Fixed bug on creating investment orders on not active portfolios

## v0.10.2
**Chore**
* [BE-71] Upgrade to scala-commons 7.0.0 and Scala 2.13

## v0.10.1
**Bug fix**
* [feature/NP-1229_logback_massive_change] Updating logback

## v0.10.0
**New feature**
* feature/NP-1207 Updating Gitlab CI pipeline

## v0.9.0
**New feature**
* [ci skip] feature/NP-1207 - upgrade gitlab ci pipeline

**Chore**
* [INF-165][INF-83] Added validation to the createOrder amount (must be gretare than zero)

## v0.8.5
**Chores**
* [INF-91] Invest remaining available cash
* In case Lisa return a 403 when there is available cash but less than the
* order amount, we ask Lisa the available cash and we retry with a new
* order with exactly the returned amount.

## v0.8.4
**Chore**
* [NOJIRA] Added sc alafmt.conf and code reformatting

## v0.8.3
**Chore**
* investment order execution job

## v0.8.2
**Chore**
* [INF-20] add endpoint to trigger investment orders execution

## v0.8.1
**Chore**
* [IMP-549] validate portfolio user

## v0.8.0
**Chore**
* Updating pipeline

## v0.7.0
**Chore**
* [IMP-528] internal delete investment orders

## v0.6.0
**Chores**
* version bump
* [IMP-527] added an internal endpoint to retrieve investment orders

## v0.5.0
**Chore**
* [IMP-526] create POST investment-orders external api

## v0.3.1
**Chores**
* version bump
* [IMP-540] Added user_id to investment_order model

## v0.3.0
**Chore**
* [IMP-525] integrated lisa api that create a cash flow

## v0.2.0
**New feature**
* Feature/imp 521 define datamodel

## v0.1.0
**New features**
* Feature/imp 521 define datamodel
* Feature/imp 520 project skeleton

**Chores**
* Updating pipeline
* [IMP-528] internal delete investment orders
* version bump
* [IMP-527] added an internal endpoint to retrieve investment orders
* [IMP-526] create POST investment-orders external api
* version bump
* [IMP-540] Added user_id to investment_order model
* [IMP-525] integrated lisa api that create a cash flow
* Initial commit

